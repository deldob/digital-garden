# digital-garden

An eleventy attempt on creating a (very) small digital garden / personal webcorner. Inspired by the Gemini sites aesthetic and philosophy, but open for more people to see.

Runs with a basic eleventy config. You'll need Node JS.

Texts are licensed under CC-BY-NC-SA, Creative Commons Attribution Non-Commercial Share-Alike
