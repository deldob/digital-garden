---
title: Garden
layout: base.njk
---

## My little (as much as I could make it) corner of the web.

I make visual stuff over at [my Portfolio](http://delyo.be). In here I talk about counter/culture, music, art, and low/hi/technology from a mainly anarchist perspective.
Digital hobbyist. If there's something exciting in my binary box, I'll tickle it. Maybe not for long, but I will.
This is a personal try at the most minimalist garden I could do, by using an SSG.

[Gitlab hasn't been the most productive](http://gitlab.com/deldob) / 

[This is what I've recently listened to](https://tapmusic.net/collage.php?user=swillfreat&type=1month&size=3x3&caption=true) /

Get in touch though email: ***sayhi at delyo dot be*** or through [Mastodon](http://mastodon.social/@dobody) maybe.

## Writings
<ul>
{%- for post in collections.rant | reverse -%}
 <li>{{ post.data.date | postDate }}<a href="{{ post.url }}"> &mdash; {{ post.data.title }}</a></li>
{%- endfor -%}
</ul>

## Stats:
*those are updated manually when I find the energy because writing an automation script is too much work for not much gain*

This site uses 0.220 MB of storage

There are 1 output files on this site

This site is currently hosted by the good folx over on [http://domainepublic.net](http://domainepublic.net)

