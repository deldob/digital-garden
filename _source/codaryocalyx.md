---
title: Music 
layout: base.njk
---

*Codaryocalyx Motorius dances.*

## Music I've re/discovered/liked/thought about

*There's bound to be more stuff here in the future.*

<ul>
{%- for post in collections.music | reverse -%}
 <li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
