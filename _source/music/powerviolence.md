---
title: Powerviolence
layout: base.njk
tags: music
---

I've started listening to a lot of powerviolence.
For those who aren't hardcore punk nerds, powerviolence is a subgenre 
that pushes hardcore punk to a faster, harder, and more screamy place. It's very angry and so am I.

- ABC Diabolo - Give Rise To Doubts
- Dropdead - Self Titled (1993)


