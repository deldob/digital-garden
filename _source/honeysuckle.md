---
title: Rants
subtitle: Rants and the like
layout: base.njk
---

This is where I scream into the void, but in a long version.

<ul>
{%- for post in collections.rant | reverse -%}
 <li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
