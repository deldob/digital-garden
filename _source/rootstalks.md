---
title: Links
layout: base.njk
---

[DIYConspiracy](https://diyconspiracy.net/) - diy, hardcore, punk, underground
I helped create a few images for this great resource for discovering DIY punk music.

[Fabrika Avtonomia](https://fabrika-avtonomia.org/) - diy, anarchist, social centre, collective
I've helped with posters for events and with my time in this autonomous cultural space in Sofia. If you ever get a chance, visit it.

[PrePostPrint](https://prepostprint.org/) - tools, code, design, documentation
A very nice initiative from designerfolk that use free software and web technologies for mainly web to print publication. A lot of resources.

[CC4r licence](https://constantvzw.org/wefts/cc4r.fr.html) - license, web, design, philo
A license that grants you the right (nay, asks for your responsibility) to seriously re-question the tools you use, where, how, and for whom.

[Luther Blissett](https://lutherblissett.net/) - art, ???
Just art world fun.

[History of 20th century UK radical printshops](http://radicalprintshops.org/doku.php?id=start) - radical, press, print, documentation
Comprehensive resource for UK radical alternative press printshops and collectives.

[Atelier Temeraire](https://atelier-temeraire.tumblr.com/Infos) - radical, design, print, atelier

[Hacker Manifesto by McKenzie Wark](https://hackermanifesto.org/en/english/abstraction/) - hacker, code, philo, documentation

[GIMP Scripting](https://opensource.com/article/21/1/gimp-scripting) - code, design, gimp, documentation

[fake halftone with web](https://codepen.io/ycw/pen/NBjqze) - tools, code
It's fake, but it looks real. And looks good.

[ToDoThisDoThat](http://tdtdt.net/) - code, tools, documentation, linux

[The Web0 Manifesto](https://web0.small-web.org/) - web, manifesto, philo

[Hostlux free/libre decentralised services](https://hostux.network/en/) - tools, web, floss

[PDF Sign, Organize, Compress, Metaedit tool](https://pdf.hostux.net/signature) - tools, pdf

[Actipedia,art as activism](https://actipedia.org/) - documentation, art

[Alice Neron](http://www.aliceneron.eu/) - nice

[Martin Lemaire](http://martinlemaire.fr) - nice, asemic

[Théophile Gerveau-Mercier](http://tgm.happyngreen.fr) - nice, web, adddiction internet, art


