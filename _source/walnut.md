---
title: Tinylog
layout: base.njk
---

## It's not an *actual* tinylog

2024-02-22: Spending way too much time figuring out how to create a "simple" bash script to update this page from the command line. Guess what tool was used for this entry... well not my bash script that's for sure!

2024-02-22: Went to workshop @ iMAL Brussels.

2024-02-21: Me and M struggled for an hour and a half to find where exactly our JS regex.exec() function was effing up in capturing otherwise valid matches. Guess what, turns out Showdown convets (rightfully so) > characters to HTML safe entities. We used > in our regex and should've used &gt; => turns out that you should CHECK THE OUTPUT every time instead of baning your head against the wall for a single character...

2024-01-16: Thinking about a hobby project to stay active. A RPi music system maybe. More costly than a music player on my PC, but less than a dedicated stereo. I need a way to listen to music that doesn't make me gravitate towards the computer. Kick that addiction in the sack.

2023-12-29: I have to create a way to automate updating my site and then pushing it to a git and to the FTP server. Using 4 programs for this when I could do it at once just takes more time.

2023-12-29: I keep gravitating towards lightweight new-old digital practices. IRC, Gemini, static sites, simple markdown. Wonder why.

2023-12-28: So I quicksaved just before an overpowered turret 3ft from me started opening fire. I keep respawning just at the second it kills me, so that's great. I get to see my character's death before I can even fully spawn.


**Past stuff**

> I am not your comrade. I may be in comradeship, by my comradeship is far from enough to define me. I may be in the same struggle, but from a different position. When you refer to me as comrade, you forget our differences, and those differences are precisely the things that give sense to a collective struggle.

The image as an end is the image as the end. The image absorbs all action, flattens out all motion, only to churn out a skin to bathe in.

> La raclette est une véritable tâche Sisyphienne. En attendant que fonde le fromage, on parle pour passer le temps à quel point la raclette c'est trop bien, comment on adore, tout en observant, agité·e, le processus lent du fromage. Une fois fondu, on mange, on parle de comment c'est lourd et comment on est plein, puis on recommence. Il faut imaginer le Savoyard heureux.

L'auteur·ice est-iel aussi actif·ve que l'écrivain·e? L'autorat suggère dans son après-souffle une relation d'appartenance dont l'écrivain·e n'est pas victime, iel reste dans une relation de pratique.

> Qu'est-ce qu'une pratique d'artiste? Que représente un artiste sans pratique, sans le monosujet qui le titille, le déchire, et le pousse à agir? Peut on parler des 'ses pratiques'? What if they practice not a practice or practices, but invividual endeavours, unflavoured by an intellectual monoculture?

Je regarde souvent mes créations passées. C'est moins un air de nostalgie que d'envie et de jalousie qui m'y emmène, je sais très bien ce que je vais y trouver. Je créais beaucoup plus de choses, aussi petites soient-elles. Je les créais parce qu'il y avait une faim, un besoin qu'elles existent pour enfin pouvoir se reposer. C'est toujours le cas, mais je ne crée plus autant. Je n'ai jamais pu élaborer un style, une esthétique ou pâte graphique, je n'ai pas de trait, pas de pratique. C'est bien pour cela que j'ai entrepris le Master, l'épeuve finale avant de me jeter dans le monde des travailleurs, celle qui durcirera la confiance de pouvoir enfin s'attarder sur quelque chose, rythmer, creuser, au plus profond. Marteler son ciseau jusqu'à ce que son bout soit aplati, ou jusqu'à voir dans le monolithe qu'on a entamé les formes désirées de son récit. **Jack of all trades, master of none.**

> C'est avec cette recherche singulière, focalisée, triangulée maintes fois, mesurée au niveau, que j'ai jeté la pioche. Un rocher de cette taille, il faut s'y attaquer avec une idée et un plan, non pas un coup de tête. Donc avant toute chose, avant tout coup porté, j'envisageais les possibilités. Un éclat à gauche, un pli à droite, le risque de briser bien plus profondément la matière qu'elle ne l'est déjà et d'en perdre la moitié, la sédimentation, et surtout, par principe et par seul principe et par plus grand principe, savoir d'où je travaillais. A force de recomposer les plans afin que mon terrain ne s'effondre pas, c'est là le ciel qui m'est tombé sur la tête. **On doit imaginer Vercingétorix heureux.**

Qu-entend-t-on par recherche? Quelle est la pratique d'un artiste? C'est quoi ton sujet? Tu travailles sur quoi, là, en ce moment? Voire pire, tu travailles quoi, là, en ce moment? Ces mots sonnent particuliers, singuliers, inobstrués par les multiples échos d'informations diverses, des craquement des papiers à bonbons de mille et une marques. Comment le font-ils? Se concentrer, se focaliser, ne pas s'éparpiller, avoir un objectif, bien cibler. Je ne me rappelle déjà pas ce que j'ai diné la veille, comment alors continuer une recherche sur deux ans? Pas besoin de scruter le fond de l'abysse pour se sentir scruté à son tour, placer son regard sur une seule chose, quelle qu'elle soit, me suffit. **Mille rochers ne pourront faire comme une seule pétale de fleur, qui m'écrasera aussitôt.**

> Je ne suis ni multi-instrumentaliste, ni pluridisciplinaire, ni média-mixte. Je suis comme tout le monde, éparpillé sur divers intérêts. C'est quoi, une recherche? Pour moi, c'est trouver parmi le désordre devant mes yeux non pas une pièce à étudier, mais un chemin par lequel passer. Ma recherche sur le graphisme anarchiste m'a fait perdre, au moins pendant un temps certain, ma foi et mon implication dans cette nébuleuse politique, autant que ma pratique de graphiste. Je n'arrive plus à concevoir ou étudier l'image, par perte de priorité. Je travaille donc autre chose. Quoi? Tout ce qui n'est pas l'image, simplement, est un potentiel d'émancipation de ce trou. Voilà peut-être la question qui gouverne ces sept-cent trente jours d'application dans l'école d'art: **Comment s'émanciper?**

On se doit d'imaginer une recherche qui n'en est pas une. Pss une recherche, mais une infinité de trouvailles. Le mémoire perdrait alors son sens imagé pour gagner son sens étymologique: la question à se poser n'étant pas qu'est-ce qui m'intéresse mais **"Qu'est-ce que j'ai foutu?"**
