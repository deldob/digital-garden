---
title: Questions and the lack thereof 
author: Delyo
date: 2024-02-07
layout: base.njk
tags: rant
---

My biggest fear in times of lectures and presentations is I can't ask questions. 
I love questions, they're the best way to learn about something, and they're also a necessary interaction between you and the lecturer.
There's bound to be a... lecture... in the time between the start and the end wherethe presenter will teach or explain how the stuff they talk about works.
But it's never enough, is it? It's barely introductory, even when every little detail is accounted for.
So you always need questions. I always need questions. I need questions because I don't HAVE questions.
So if an audience member could help by, uh, asking, they'd do me a great service.
What is there to ask after you've just learned about something?
I mean, what's there to ask about the precise subject of the talk, once it's been given?
My opinion is people who ask the questions, in front of everyone too, like they don't think they'll be burned alive if they make a little mistake, have prepared.
Hell, they've probably read the book the lecturer just wrote or specially read up on that stuff before the talk so they'd have questions to ask. The audacity.
So I like questions, I like that people ask questions, I'm just not entirely convinced their motives are pure.

Barry wakes up, checks his newsletter and sees some author has written some book about some subject somehow connected to something Barry saw some day. 
And now he's coming to shed some light on the subject in some library sometime this week.
Hell, Barry thinks, I should check that out, sounds somewhat interesting.
But barry doesn't know the first think about this precise book, he thinks, he should reasearch it, he thinks.
And so he does.
And Barry somehow now knows something more about the subject, and he's going to lay low until someone opens the questions moment and have the audacity to demand more than what was just given.
You've seen and read and looked up and researched the thing, Barry, thinks EVERYONE, why are you down here?
Barry has the guts to stay in front of an author he knows about, saying things he knows about on a book he knows about.
Should've waited until question time to come in, should've been honest about his knowledge. No one likes a faker.
And question time comes, and Barry asks the first question, and waits a little after the answer, and then when no one asks a question, because they're all full on what the author has said they they didn't know the first thing about, he asks another.
Well that's cheating. What are you in here for if you didn't learn anything from the lecture, Barry?
Now everyone knows his shame, he came here knowledgeable, and still sat through, as if he pities the author.
Barry prepared, came prepared, went out prepared.
A lot of people learned thiungs from Barry's questions.
But Barry sucks. At least people were honest, they didn't come here waiting for Barry's questions, they hapened to stumble upon them.

Okay, I'm not entirely hating Barry. Where would I be without him?
People who ask questions either prepared, which sucks out all the fun from being hit smack-dab in the face with a truckload of new information (very fun) or they happened to think about the truckload of new information while being hit in the face with it.
How come they don't feel the pain of it?
Whenever I come out of a talk like that I go for a beer to unwind.
I need to marinate my truckload-slapped face in the sweet nectar of slacking off, better known as a beer, ale, pint, or drink.
I need my thoughts on the subject to form after the fact. How am I going to think about something if I can't feel it in my bones first?
It takes time to develop feelings from a lecture, too, enough at least to so bother your daily activities later that you must find an answer to stop it.
That's when you find out what to say, that's when the questions come in, armful at a time.
You've heard the stuff, and the stuff has come into your life so strongly that you now have some feelings on the stuff, and those feelings need closure, like any oher feeling, and closure comes in the form of answers, which would not exist if it weren't for questions.
Any intellectual discourse partaking in a dialectic, dialogue, conversation, exchange, or anything outside of the monologue, is about feelings.
Only the mind alone, left to its own devices, cut from the outside world and focused on one task exactly can formulate a non-feeling thought.
Those usually suck, by the way. 
