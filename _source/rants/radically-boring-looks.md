---
title: Radically Boring Looks
author: Delyo
date: 2022-12-03
Robots: noindex,nofollow
layout: base.njk
tags: rant
---


*This blog post officially appeared on my other blog [A Gopher's Ballad](https://agophersballad.wordpress.com/2022/12/03/radically-boring-looks/). Since then, a few questions and outlooks on the subject have changed. However I'm still integrating it to centralise my thoughts and for retrospective purposes. Read with care.*

---

Throughout the years I’ve been interested in anarchism and in radical movements, I’ve seen one thing that’s disturbed me perhaps more than any other: the aesthetic. That’s probably the least important part of such a political movement, the major problems being the outreach of its ideas, the words used, the concern of being up to date with the issues the movement is tackling, and mostly, the very large-scale infighting most left movements see themselves victims to. Nevertheless, as someone whose primary occupation is interrogating design and communication, that’s what struck me, and it’s also an issue I can consider researching, having some experience in the matter.

That’s not to say that it’s a complete hellscape, because it isn’t. I’ve searched multiple anarchist archives to see what the chronological evolution of anarchist propaganda looks like, and if it exists. And it clearly does. We’ve gone from revolutionary realism and very illustrative symbolism to more creative narratives, merging contemporary art styles, and subtle approaches. From newspapers that look like, well… any other newspaper, to self-published collage zines and federated publications in a multitude of styles, not to mention often a researched look in websites. But the latter is the case for a minority of the radical communication channels. Mostly, we’re (in my very non-omniscient opinion) stuck in a sort of historicity of previously used aesthetics.

Here’s what I mean: red and black, raised fists, “punk” fonts (whatever that means), flags, etc. If it rings a bell, it’s because a majority of the visuals released for radical use took some of the most expressive pieces they saw, and reuse them to this day.
(A brief tangent; I also can’t understand is this adoration and infatuation with the Masters of anarchism, all the 19th and early 20th century figures that produced radical theory. What’s with the Kropotkin pin? Didn’t we scream “No Gods No Masters”, or are old dead guys exempted from this principle?)
The world has greatly changed, and we keep quoting 19th century slogans and using 19th century symbolism. We keep on using sharpie fonts and half-assed collages for the covers of our zines as if we were creating them in an Irish squat not two rooms away from a Conflict concert at the beginning of the 80s. Don’t get me wrong, the core of the ideas is still relevant and is especially constructive in the production of theory, but the visual message this practice sends screams “I don’t know what decade it is.” And since we’re making propaganda, the idea isn’t to get together in an anarchist-convert circlejerk, but to put the ideas and questions out there, for those who haven’t yet questioned hierarchy, oppression, and power structures. And for what I’ve seen, it tends to scare people away with its connotations of violence, destruction, and a look sometimes way too close to soviet propaganda. Maybe we ought to rethink how to reshape, and we can reshape how we invite others to rethink.

This isn’t a call from a self-appointed patron saint of radical graphic design. It’s merely an observation from someone tired of seeing a punk anarchism revival in the visuals, even though those are two things I adore. I don’t pretend to know what tomorrow’s pamphlet should look like, but I know I can see some mistakes in today’s.

Maybe let’s stop concentrating on the portraits of writers and theorists. They contributed greatly, but through text, not through their black and white, over-beardy mugshots. We should also figure out how to introduce a subtler way to communicate our ideas, for the readers who might be into restructuring society, but have a mellower, calmer sensibility affected by hope instead of rage. Perhaps it’s time to stop adopting the same typefaces as any commercial branded publication, and shake things up a bit, but with today’s tools? There’s libre fonts, experimental ways to restructure the reading experience, and web design practices that stray away from the [menu – featured articles – content – footer] website to engage users in exploration and questioning of their web experience. I’m suggesting we should make the propaganda object an interesting object-in-itself, instead of screaming “This is a 4 hour read of bland propaganda put together in ten minutes in the font you’re used to see absolutely everywhere on unfinished documents!”

Make us want, make us scroll and click, make us turn the pages frantically to see interesting quotes and new ways to look at the world! Make the radical look radical, and take pleasure in learning how to do it yourself!
