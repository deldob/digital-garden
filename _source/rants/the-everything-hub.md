---
title: The Everything-Hub
author: Delyo
date: 2024-01-14
layout: base.njk
tags: rant
---


When I set out to find platforms that could help you build a personal site for free, one of the recommendations I got was Deta Space. So I went and created an account, curious creature for technology that I am. After ten minutes, I wasn't well convinced by this shiny new toy, and might be even less so after I read an article for them in The Verge. But instead of singularly being mad at one company for doing one thing, I'm just a lot more worried about the underlying situation this poses.

As a simple matter of illustration, this is what you get when you start: A blank canvas with a few buttons and an editing menu. Scrolling is horizontal, and your editing menu allows you to draw rectangles - in which you decide what content resides - for a YouTube embed, a picture, music, or a program like a file manager or a paint tool. Additionally, if you find no program to your liking, you can prompt an AI to create one - although that feature is not as powerful as we might want yet. You won't create a powerhouse video editor, but you can ask for a slideshow of pictures with a pink sepia filter, a calculator that spells out the numbers in cyrillic (I'm guessing), a minesweeper with Hello Kitties and Steven Segalls. You can create notes and pin them to your home canvas, etc.

And in all honesty, this seems fun. Having the ability to create simple apps which you need in a small timeframe is definitely something worth keeping for most people. The design is sleek, the experience is simple and intuitive, and the horizontal scroll canvas seems like an interesting tool to tell a story every time you use your computer.

## What wronk, why you burning?

That's unfortunately where the positive aspects end for me, though. Reading about the project, I understood it's an effort to bring the first personal AI-powered cloud computer out there. And that seems profoundly like an oxymoron. Because it is. Not to a large extent, not even with only a couple of exceptions: it is either your personal computer or is in the cloud. Granted, Deta don't envision their software like other cloud services: each user has a separate space on which apps are installed and content is created. It's many little virtual computers, on each of which programs are individually present, and not just hivemindedly connected to the big main brain.

But those computers remain in the cloud, don't they? Unfortunately, as web2.0 bloats up more and more, akin to a corpse in its accelerated decomposition, the energies of resistance towards the new shiny toy that is Web3 (an abomination of a creature, yet another way for cryptobros and their smug fanbase to make sense of the fact that they bought into a pyramid scheme) seem to forget what was always there in the first place. Our computers have always been personal, whether we use them for an hour a day or for more time than we sleep they contain information about how we deal with information. The files per se are not important, what we do with them, how we arrange them and connect them seem to speak volumes about us.

## Landscapes

A Sasuke wallpaper, stretched to its maximum capacity to the lateral edges of the screen, looks at the back of a pile of icons flooding the desktop, each a colorful stroke of pixels. Counter Strike, Team Fortress, Need For Speed and broforce are crammed next to the front door of a studio where Photoshop CS4, Picasa, and Autocad lie in the company of the good old Winamp Boombox. The downloads foder is filled to the brim, periodically seeing its contents either put in the Pictures folder if the user hits a self-conscious curb, or in the Trash if they just can't be arsed.

Over the default green XP hill hovers a window into a pedantic Diogenes' little world of data-hoarding, never to be touched, only observed periodically to counteract any entropy that might ensue. Folders are named A-K, L-P, P-Z, ZZZUnsorted, harboring not unlike matrioshkas a hundred other subfolders, numbered, dated, labeled to look pretty.

A Gnome application dock stands with its populated army of app shortcuts against a lone Trash icon on a deserted desktop. The desktop theme is not changed in the slightest, for fear that once out of its pristine package it will lose its value.

## From hub of everything to everything-hub

So much transpires into the home folder of a personal computer. Unfortunately, as for a while now a computer has served little purpose if not connected to the internet BECAUSE we want to put stuff on the cloud, we find ourselves now in a position where NOT putting stuff on the cloud makes little sense because the computer is made for the internet.

The Deta Space project seems to suggest that the next step isn't the futility of not connecting to the Internet, but the madness, so 2005-ness of not having all of your belongings on the cloud.

These energies that could have gone towards actual decentralisation, a back to the basics philosophy of using only when you need, connecting peer-to-peer, making apps that do one thing well and can work together, seem to go to a place where everything seems to always run, forever, and be a hub for everything. No wonder younger users know less and less about directory structure and find their individual files mainly through the search bar. Every app has its cloud space, quite literally misty and cloudy, where a handful of particles, call them files, hang suspended, sorted by Most Recent and by nothing else. Every app now incorporates chat AND transfer AND streaming AND creation AND games.

Is this really how it all goes down?
