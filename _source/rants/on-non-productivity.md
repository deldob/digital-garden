---
title: On "non-"productivity
author: Delyo
date: 2023-06-03
layout: base.njk
tags: rant
---

I recently read [a post about productivity](https://dostoynikov.bearblog.dev/fuck-being-productive/) on bearblog. To paraphrase very broadly, the author affirms his decision to stop nagging himself to "be productive" after work by engaging in activities such as programming, learning a language, etc, a decision to feel better about "doing nonproductive things" like watching shows and just spending time with his family and friends.

Can't really disagree. You shouldn't kill yourself trying to live more "productively". Take the time to do nothing, whether with friends or alone. Sleep in on Sundays. Binge that show. Play a couple of games. There's a few comments to make here however.

All of this isn't "non-productive". Whatever you're doing, you hair grows, your nails grow,you produce saliva, you go to the toilet and leave stuff there. And if that's not productive because it's just a transformative process, then what is, what activity requires no materials to be ingested in a way in the first place?

You're productive when playing games,you're training a certain skill of your brain, which inevitably leads to other things than just games. Your identifying wih TV characters makes you reflect on your own life, and dissociation from other characters helps you wonder about "otherness". To put it briefly, you're never truly, universally "non" productive.

Here's the second thing: think about your job and how productive it is. Wonder if it really is productive. You spend 8-10 hours on average for your job, including transit to and from home. Consider how much of that time is taken up on the ground of doing something productive for society. Consider how much or how little of it is really helpful. How much of it on the other hand aligns with the idea of bullshit jobs put forward by David Graeber? There you will find true productivity. And I'm not just saying quit your job, but if you're having a dilemma and guilt about your own time, please think about the real culprit: growth,infinite growth. Growth of speed, of range, of items, consumption, technology, "ease of use" gadgets.

If you get angry at anything, let it be not the feeling of not growing, but the structure giving that feeling. Growth is what dried out the Aral sea, cut down centuries old forests, created mass spending, useless needs, made us quantify relationships through likes and reactions, and made us so annoyed with public life that most of us are disinterested in anything happening outside our immediate vicinity. Be angry at those who pressured you to grow when maybe you needed to get more rooted, maybe needed calm, less. Maybe you needed to hibernate, instead of commuting in gas-infused concrete paths. Maybe you needed to learn how to live slowly instead of having a start-up sell you the product or the training plan to do that easily. Just maybe...

This isn't by far any set of rules,neither is it a full exploration of daily productivity. But hell, to get better, understand that you're not the only actor who influences the state of things. So take those hours off-work, and play, read, watch, and blab. Take them with the knowledge that every minute spent doing them is a blow to this growth which ironically pushed you back into a corner of self-doubt, white cubicles, click-to-be and Baudrillardesque rewards. Knowledge is the first of all baby steps.
