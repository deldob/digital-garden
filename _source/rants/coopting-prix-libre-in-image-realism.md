---
title: Coopting prix libre in image-realism
author: Delyo
date: 2023-09-20
layout: base.njk
tags: rant
---



*This post first appeared on my bearblog.*



Four years ago, on a November night, I was meeting up with an old friend in a rusty cultural space on the banks of the Brussels canal. I had never heard of Barlok before, but the view was promising. Bright colored neons, twisted and screwed and brazed together, hovered over a two-door warehouse entrance. under that stood two people behind a small, beaten up desk serving solely as support for a few bottles of beer and juice and a small toolbox ridden with stickers.
 
> \- How much is the entrance?

> \- However much you decide.

Dang. What was that? An introduction to [prix libre](https://diyconspiracy.net/prix-libre-the-gift-economy-of-diy-punk/). There I was, figuring out how much I could afford to spend, how much I would need for beers once I'm inside, how much is necessary, and how much is customary. That's a lot of math to do for a night of punk.

> \- Well what's the recommended price?

> \- Donation. And there isn't one, three or five or ten points, whatever.

I shyly pick out a few coins from my pocket and look towards my interlocutor with a cautious look, ready to receive a disappointed and judgemental look back for what feels like an eternity, but none of that happens.

> \- Come in. Have a nice evening!

The smiling *entrance-person* (wish I could find a better term) extends his arm towards the inside.

That was the first time I learned what *prix libre* actually means. And I'll cherish it, as this same place was closed not a year later, because someone finally got the permits to build something in lieu of the warehouse complex that had served for so many as refuge, as a cultural coup-de-coeur and as a space to share their art. Since then, I've seen a handful of places operating at prix libre donations, all of which turned out to be squats and very low-money social centers, harboring the ideals of non-hierarchy, openness, tolerance and freedom from profit-centered actions.

And now it's all becoming blurry to me. Prix libre is a philosophy, not a pricing model, and yet as our world so unscrupulously allows it to happen to many meaningful practices, it's become an aesthetic value, just like the brazed and DIY neons, the somewhat-grungy looks of those spaces, and the graffitis crawling around the area like vines on a quest to bury cement. It's become "cool", part of a "brand" *(God forgive me)*. A corny self-derision bumper sticker that makes the driver behind it think how cool and relax the Opel Astra wheel-holding commuter in front of them is.

It all says "we don't *want* you to spend money if you don't have it, but it would help." A farce to make any hipster and bourgeois-bohème acid techno kid *with a cool 80s jacket gotten from a severely overpriced second hand store* (but hey they *handpick* the clothes that's why there's a 50% profit margin) pay a little more than they would have. Especially if there's a listed recommended price. Research shows that donation-based entrance with recommended price tags tend to make a little more money on average. After all, you don't wanna be a sub-par contributor, do you? **That's not the recommended price,** says the angel in your head, **that's the minimum. Be generous, friendo!**.

> [At what price would you price free price if the free price could free price?](https://en.wikipedia.org/wiki/How_much_wood_would_a_woodchuck_chuck)

It's not like it's a giant conspiracy to steal DIY radical tactics from small orgs to make them unusable, that the event promoter-industrial complex perpetrates. It's usually not even intentional on the part of bigger organizers. But in a present where image and aesthetics precede context in their importance, any facet of anything can be taken out of its surroundings as a cool-factor to add to a wholly different ensemble. And so this practice, entirely based on solidarity and principles of openness, which worked to allow anyone access to culture and at the same time supported the orgs that promoted it just enough, without profit, finds itself on the marketplace of images and "skins".

The other way around, this de-contextualization hurts DIY and not-for-profit cultural spaces inside their own doors, when it loses the meaning of "pay what you can and want" and starts signifying "the minimum is zero". [Recently, there's been conversations in a few Balkan cultural spaces and collectives about the support habits of the audience.](https://diyconspiracy.net/worrying-instead-of-donating/) Slutbomb's concert in Smederevo, Serbia, saw about 60 euros donated at the end of the concert. In Sofia, even less was donated by a total of 60 showgoers. That's barely enough to cover the gas for an act's one-day driving to the next place, what's to say about food and other expenses. If DIY venues get used to this, they won't be inviting any non-local bands they might send home on their own bucks, and will probably be less operational as a whole, to put it mildly. This hurts.

So where do we go from here? I haven't figured it out, and I'm assuming a truckload of organizations are having a hard time juggling with this issue. In the meantime, put up posters about this practice and the values of your spaces, ask the vocalist to say a few words between songs, distribute leaflets, pause the show or the talk to inform people, and trust in their solidarity. Mutual trust and mutual aid is the only way we'll get farther on this earth.
