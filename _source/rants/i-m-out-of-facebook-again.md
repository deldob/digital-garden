---
title: I'm out of Facebook (again)
author: Delyo
date: 2023-09-28
layout: base.njk
Tagline: oh *crap*
tags: rant
---

There it is. Once again, for the second time in my life, I disabled my Facebook account. It's only been 3 days, and I'm already feeling the cold turkey side effects pile up. See, I'm a gen y - gen z kid, but I never quite got the social aspect of Instagram. I know everyone's over there, but hell. To me, Facebook's been the home for internet sociability since I was 11.

I used to scream in the void, then I got some reactions, then I started sharing memes, tried to fit into the typical nice Facebook profile. I got my mother and my grandmother to use Facebook, I kept a handy list of contacts there, and got to see who a person was by progressively seeing their posts pop up on my news feed. I made about 20 pages that collectively have no more than 50 posts, kept all my communications there, followed trends (remember the profile pic and cover generators that appeared when the cover picture feature was first implemented? I was one of the earliest adopters of the "cool way" to do it). In the last few years I marketed a zine there, talked about my graphic design works, sold posters, and created a careful public repertoire of the music I listen to. It's been my internet go-to page for 15 years.

I want it to end. It's never actually been incredibly useful. It was fun and handy for a few things, you didn't need to write emails to your friends anymore, and you could keep in contact with your favorite people without actually having to nag them about their week every time. But it's never been really, wholly useful. What Corey Doctorow calls the "Enshittification of the web" is heavily felt. Most of what I see when I scroll down nowadays is trash. It's shitposting pages "suggested for me" (leave me the fuck alone, algorithm), it's controversial news mining engagements from me, it's political echochambers slowly building up, and only a small portion of it all is about my friends. And I mean actual friends, not the Facebook relationship thing that says "only add people you know in real life" but that no one follows.

I go there to "unwind" and to have a laugh, and I stay because the infinite scroll is so, so hypnotic. I'll have read twice the volume of Tolstoi's War and Peace on Facebook posts before I even finish a pulp novel's 4 page chapter. Now I don't know what you call this, but I call it an addiction. And ending it is not about winning back "precious time", but about spending it better. I want to read blog posts, I want to read a few more books a year, I want to have a real long paragraph discussion, or fully focus on an album. I want to unwind without having brain rot form up in my cortex and without my fingertips tapping a sly response to some royalist douchebag who just insulted trans people and made me *have to* engage with him.

Yeah, I want action, I want my imagination to dream up worlds before those are cartoonishly drawn in the form of wojaks or deep fried mémés of Juan, the horse on the balcony. I'm not about to emerge from my cocoon a born again sigma stale patriarchal stereotype. That's not the idea.

But it's stronger than me. I can't count the times I've written "fa" in the searchbar and pressed enter knowing that the autocomplete is fully aware of my wishes and will take me to the big blue portal with a nice centered feed, only to be welcomed by a sign up page. Hell, I came back to post on Mastodon for god's sake. And I don't even know my friends' profiles on there!
**So now I'm writing this.** I still need to get my thoughts out, but at least this way there's not "heart react" (gimme a break) or comment option on my post, that I have to wait for people to click.

I know that in order to change a place that's become a monopoly, there's no real alternative but to invest it, to go in there and break it up from the inside. Say what you want, but people won't see posts about the dangers of *social network platform capitalism* **if they aren't on the platforms.**

But personally, I need a break. Perhaps I'll come back, I hope not too soon, may I give myself the strength for that; in the form of a bare bones profile, existing only as a way to get the two good berries from the pile of otherwise burning garbage on this "social" network. The web never bothered me anyway.

