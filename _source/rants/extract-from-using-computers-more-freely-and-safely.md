---
title: Extract from "Using computers more freely and safely"
author: Delyo
date: 2023-10-02
layout: base.njk
tags: rant
---

Extract from [Kartik Agaram's article on using computers more freely and safely.](http://akkartik.name/freewheeling/) Go check it out because it gets even better when read fully. Jump to end of quotes[^1] for my few words in footnotes.

> One coping mechanism is an idea from almost 20 years ago called situated software. This is software with a few users who know each other and share a social context or situation. The idea is analogous to local government: services with few stakeholders are often better able to meet the needs of their stakeholders, particularly when everyone knows everyone and there's a certain level of social accountability.

> This isn't always possible. Sometimes we have to use software with millions of users. But I think it helps to rely on small-scale tools as much as possible, and to question tools for large crowds of people at every opportunity. [^2]

![Dependency by Randall Munroe](https://imgs.xkcd.com/comics/dependency_2x.png)

> The idea of situated software has been seeing a renaissance recently, and there is growing awareness of the value it brings. However, I think we don't yet appreciate the implications of this idea at all levels of activity around computers.

> As a programmer, I've tried multiple times in the past decade to create services just for myself and a few friends. Each of them has fallen away after a year or two. And a big reason for that was the burden of keeping up with updates for all the tools they depend on.

> It took me a while to realize the underlying issue: I was using tools that themselves are catering to non-situated software. They assume that it's worth someone's time to keep up with updates every month or two. But situated software doesn't have those kinds of surpluses. It requires programs to just work once built, with little maintenance.[^3]

---

[^1]: That's a great article that I very much like, but I'm still going to ramble on about the subject because this is my blog. Fight me.

[^2]: Questioning large-scale software is sort of a Prime Directive for small/slow/alter/indie web users, who by the way are also its creators. Implementing small-scale or local software is also one. But one ought not to forget that vendor lock-in is a big factor. If all your friends only have Facebook, you'll have to use it. If your friends aren't at ease with computers, they'll use the easiest *(seemingly the simplest, but often the most complex)* tool they can find. How can you convince someone who doesn't */care to/* know what "federated" means to switch over to a federated tool? With that in mind, we can and should still try to talk about digital dependence and sobriety, wash in ecological concerns, and (only if your interlocutor is so inclined) criticize the market and capitalist aspect of large-scale platforms.

[^3]: The percentage of programs that really need to only be built once is *gigantic* -> I don't need a text editor to be updated at all times, I need it to do one thing, and do it well. My fontforge font editor draws curves to create fonts, and that's it. I could use the last update just as I could easily install a very old version, and the two do the same thing with minor difference. Perhaps the one thing that should be kept updated depending on the scope of your software is security and privacy. If you're running an app for you and five other people to exchange funny gifs or poetry or links, you haven't got much to be scared about, but if it's cospending, talking about private stuff, and enabling any access to your personal files, that's another story. Either way, many apps can and should adopt a minimal-update philosophy: change things when they don't work. Don't make it impossible not to change things when they do work.
