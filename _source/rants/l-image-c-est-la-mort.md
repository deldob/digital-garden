---
title: L'image c'est la mort
author: Delyo
date: 2023-10-25
layout: base.njk
tags: rant
---

Je ne sais pas quoi faire. Après tout, l'esthétique et l'image semblent peu importer aux anarchistes, et je pense que ce sentiment et ce point de vue deviennent de plus en plus compréhensibles pour moi. Il y a dans l'image, qu'elle soit singulière ou multiple, traitée avec soin ou non, une qualité statique qui fige tout dans son cadre et rayonne une staticité autour de soi aussi. L'image est un moment dans le temps, non pas son accumulation. Il n'y a pas d'image qui traduise un procédé, il y a seulement des images qui, après le procédé de leur création, se retrouvent à éteindre celui-ci. La fin de la création est la mort du mouvement, et une image en est la tombe. **Re-présentation.** Ce qui s'est présenté une fois déjà tombe dans les ténèbres, c'est là ou l'image conçue comme une représentation tente de relever le corps, le cadavre d'un geste, rongé par les scissions qui ont suivi le mouvement. C'est à la mémoire d'un défunt qu'on érige une image, c'est dans son absence qu'on la regarde. Mao, Staline, Lénine le savaient bien. Les familles se retrouveraient-elles à penser ces derniers comme des monolithes, des murs d'idées, s'ils n'avaient pas l'obligation culturelle d'ériger leur portrait dans leur foyer? Non, ils verraient ces pères de la misère rouge comme ce qu'ils sont: des humains qui traversent le tissu de l'existence en se pliant et se mouvant comme les connexions synaptiques dans les plis de notre cerveau, jamais statiques, toujours malléables. Surtout, emprisonnés dans l'écoulement du temps, forcés à travailler les changements qui les traversent. Il n'y aurait pas de pensée à une personne sans son image statique.



Alors pourquoi les anarchistes s'efforcent de faire de même? Leur image aussi est une image de la mort. On commémore avec des bannières, des drapeaux, et des portraits des maîtres à penser. A quel but sinon celui de se rappeler de leurs leçons? Mais celles-ci ont été faites il y a longtemps, et ce n'est pas que le passage du temps qui remet en cause leur place. Les contextes sont vastement différents, quoi qu'en disent les anarchistes-même, en placant des néo- devant féudalisme, royalisme, libéralisme, etc. On s'efforce de créer des liens entre hier, il y a 50 ans, et aujourd'hui, pour pouvoir changer demain, pour que dans 50 ans on n'ait plus à le faire. Oublie-t-on la validité historique d'aujourd'hui? Pourquoi comparer notre condition actuelle à celle du début des années 1920, le monde et ses évènements contemporains ne sont-ils pas assez lourds, assez grands et significatifs que pour tenir sur leurs propres pieds? Pourquoi faire revenir des images d'autrefois, soient-elles faites aujourd'hui ou non, alors que nous nous rendons devant un monde qui est animé, qui vit? Pourquoi vouloir y plaquer des images qui le représentent alors qu'il est là et qu'il bouge?


Toute image est hors de notre contrôle. Une fois faite, elle est un paquet d'informations, et nous ne pouvons rien y oter ni ajouter. ~~Elle ne changera rien en elle-même non plus.~~ Nous n'avons que le contrôle sur ses moyens de diffusion et sur ses ancrages physiques, à savoir une matière sculptée, un papier imprimé, ou un fichier numérique. Quand on a érit "a" sur la feuille, c'est que nous avons arrété de le faire, que notre main n'est plus dans ce geste. L'encre de notre plume sur le papier tente de faire revenir ce moment, de le suggérer, ou de faire pratiquer son souvenir.


Les anarchistes ont raison de ne pas se fier à l'image, car celle-ci les arrêterait. S'ils s'efforcent de se présenter à traver elle, ils ne verront que ce fameux cadavre, comme se regarder dans un miroir, l'inabilité d'affecter le visage en face librement. A tout moment on pourra publier une nouvelle image, sauf que celle-ci sera tout autant morte. Ce n'est qu'un instant qui y est présent, et l'instant n'est pas un écoulement de temps. L'image du mouvement anarchiste n'est que l'aspect de sa mort. On ne peut plus agir si on devient image, si on devient symbole. Une affiche "nous sommes tous antifascistes" ne dit que "fut un temps, nous pensions que nous étions tous antifascistes" - elle ne dit rien sur le présent. Hormis son utilité nostalgique, une affiche anarchiste n'a pas plus de raison d'exister. *Dès l'achèvement de sa création elle n'est plus que matière pour les historiens, qui inventeront des récits sur le "comment, quand, où, pourquoi" elle fut.*


Elle deviendra aussi un grain de sable dans l'arsenal de la récupération des images, pour que d'autres qui ne l'ont pas créée se fassent paraître pareils à ceux qui en revanche l'ont créée.


*Tout celà est certainement très négatif, pessimiste et voué à ne faire découler que la tristesse.* Je le ressens, et c'est bien voulu. Comment travailler la mort? En faire découler une utilité? Une poésie? Un parnassiennisme Gautiéresque?


Peut-être que travailler l'image graphique anarchiste me mène plus fort que toute autre recherche à creuser la tombe de cette dernière. Peut-être que ma recherche de master en design graphique n'est qu'un long mot d'adieu de deux ans à cette discipline.


Pour faire revivre l'image, *ou plutôt faire vivre une nouvelle,* il ne faut jamais la terminer, ne jamais lever son crayon du papier, pour que ce premier soit violemment percé et gratté par ce dernier, sans jamais pouvoir se reposer. Comme les murs-hôtes de graffiti, où un geste efface la représentation d'un autre qui a eu lieu auparavant, le sauvant ainsi de sa mort figée. Il ne faut pas laisser l'image échapper au mouvement, si l'on souhaite toute autre chose que la mort. A bas le A dans le cercle, à bas le chat noir, à bas la figure de la main de l'ouvrier, de l'insurgé, *déchirez le portrait de Malatesta, de Makhno, de Ferrer* et laissez-les vivre dans l'anonymité de notre pensée. Ils ont éxisté, leur existence a été forte, comme celle de tout autre être humain, assez grande et assez conséquente pour n'avoir aucun besoin d'être **re-présentée**. Faites-vous vivre vous-même, organisez et luttez au lieu de présenter, bon dieu.


Je suis colère, je l'entends, je l'assume, et c'est parce que je suis colère que je peux aussi **être** et ne pas me représenter. Ou enfin, je pensais avoir été colère quand ces mots ont été écrits, vous n'en saurez rien de plus à travers ces mots.
