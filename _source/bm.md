<!DOCTYPE NETSCAPE-Bookmark-file-1>
<!-- This is an automatically generated file.
     It will be read and overwritten.
     DO NOT EDIT! -->
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<meta http-equiv="Content-Security-Policy"
      content="default-src 'self'; script-src 'none'; img-src data: *; object-src 'none'"></meta>
<TITLE>Bookmarks</TITLE>
<H1>Bookmarks Menu</H1>

<DL><p>
    <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1708557623">DESIGN</H3>
    <DL><p>
        <DT><A HREF="http://wax-studios.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Wax Studios</A>
        <DT><A HREF="https://kellenrenstrom.com/Wax-belt" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Wax belt - Kellen Renstrom</A>
        <DT><A HREF="http://takeawalkonthewildside.rietveldacademie.nl/page/screening-programme" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Studium Generale | Take a Walk on the Wild Side | GRA + Rietveld Uncut</A>
        <DT><A HREF="https://www.newweirditalia.eu/misto-mame/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Misto Mame | New Weird Italia</A>
        <DT><A HREF="https://skvot.io/ru/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">SKVOT - Online Courses in Advertising, Film and Art</A>
        <DT><A HREF="https://speakingmachine.boook.land/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">BOOOK.LAND - FALMOUTH EDITION</A>
        <DT><A HREF="http://iraivanova.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Ira Ivanova</A>
        <DT><A HREF="http://otherforms.net/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">OtherForms</A>
        <DT><A HREF="https://www.kadenze.com/courses/introduction-to-programming-for-the-visual-arts-with-p5-js-vi" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Course Dashboard | Kadenze</A>
        <DT><A HREF="https://workwithus.io/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Work With Us | Home</A>
        <DT><A HREF="https://godly.website/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Godly Website — The Most Godlike Web Design Inspiration</A>
        <DT><A HREF="https://hackersanddesigners.nl/p/About" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">About • H&amp;D</A>
        <DT><A HREF="https://spectorbooks.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Spector Books</A>
        <DT><A HREF="https://designsystems.international/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Design Systems International</A>
        <DT><A HREF="https://ohezin.kr/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Hezin O</A>
        <DT><A HREF="https://suzannebakkum.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Suzanne Bakkum</A>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">BXL DESIGN AGENCIES</H3>
        <DL><p>
            <DT><A HREF="https://www.tofagency.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Tof Agency — Digital Creative Studio</A>
            <DT><A HREF="https://dogstudio.co/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Dogstudio. Multidisciplinary Creative Studio.</A>
            <DT><A HREF="https://www.stoempstudio.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Stoëmp Graphic Design Studio</A>
            <DT><A HREF="http://www.everythingisfun.com/digital-creative-studio" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Everything is Fun - Digital Creative Studio</A>
            <DT><A HREF="https://www.1md.be/contact" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Contact 1MD</A>
            <DT><A HREF="https://www.basedesign.com/work/studio-brussel-stu-bru" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">StuBru — Base Design</A>
            <DT><A HREF="https://www.basedesign.com/jobs" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Jobs — Base Design</A>
            <DT><A HREF="http://www.klar.graphics/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Klär.graphics by Claire Allard – Graphic design studio based in Brussels</A>
        </DL><p>
        <DT><H3 ADD_DATE="1665056583" LAST_MODIFIED="1707309659">Design Studios</H3>
        <DL><p>
            <DT><A HREF="http://alliage.work/" ADD_DATE="1665056592" LAST_MODIFIED="1707309659">ALLIAGE</A>
            <DT><A HREF="https://maximage.biz/" ADD_DATE="1665056606" LAST_MODIFIED="1707309659">Maximage</A>
            <DT><A HREF="https://hubertus-design.ch/" ADD_DATE="1665056624" LAST_MODIFIED="1707309659">Hubertus Design</A>
            <DT><A HREF="http://www.imagecontext.com/?p=1&l=2" ADD_DATE="1665326286" LAST_MODIFIED="1707309659">ImageContext : Design is visual intelligence</A>
            <DT><A HREF="http://rhythmus.be/" ADD_DATE="1666400098" LAST_MODIFIED="1707309659">Rhythmus.be | graphic design</A>
            <DT><A HREF="https://typografix.welfringer.lu/" ADD_DATE="1672239598" LAST_MODIFIED="1707309659">Studio Michel Welfringer</A>
            <DT><A HREF="https://www.hort.org.uk/" ADD_DATE="1676393905" LAST_MODIFIED="1707309659">HORT</A>
            <DT><A HREF="https://www.amelie.tools/" ADD_DATE="1676835798" LAST_MODIFIED="1707309659">Amélie Dumont</A>
            <DT><A HREF="https://liebermannkiepereddemann.de/impressum-und-datenschutz" ADD_DATE="1679415791" LAST_MODIFIED="1707309659">Liebermann Kiepe Reddemann</A>
            <DT><A HREF="https://standenatris.com/" ADD_DATE="1679415803" LAST_MODIFIED="1707309659">Stan de Natris | graphic design</A>
            <DT><A HREF="http://www.bartdebaets.nl/" ADD_DATE="1679415825" LAST_MODIFIED="1707309659">Bart de Baets</A>
            <DT><A HREF="http://fanfarefanfare.nl/category/fanfare-out-and-about/o-overgaden-website" ADD_DATE="1679415843" LAST_MODIFIED="1707309659">fanfare</A>
        </DL><p>
        <DT><H3 ADD_DATE="1665056758" LAST_MODIFIED="1708557606">Design Tools</H3>
        <DL><p>
            <DT><A HREF="https://colorlibrary.ch/#red-green-blue-2" ADD_DATE="1665056737" LAST_MODIFIED="1707309659">Color Library</A>
            <DT><A HREF="https://fontsinuse.com/" ADD_DATE="1665163090" LAST_MODIFIED="1707309659">Fonts In Use – Type at work in the real world.</A>
            <DT><A HREF="https://addons.mozilla.org/en-US/firefox/addon/zjm-whatfont/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search" ADD_DATE="1665163106" LAST_MODIFIED="1707309659">WhatFont – Get this Extension for 🦊 Firefox (en-US)</A>
            <DT><A HREF="https://typecache.com/" ADD_DATE="1665326272" LAST_MODIFIED="1707309659">type foundries in the world | TYPECACHE.COM</A>
            <DT><A HREF="http://scriptorium.blog/" ADD_DATE="1666400091" LAST_MODIFIED="1707309659">Scriptorium.blog</A>
            <DT><A HREF="http://rhythmus.be/md2indd/" ADD_DATE="1666400105" LAST_MODIFIED="1707309659">Markdown to InDesign</A>
            <DT><A HREF="https://markdown-it.github.io/" ADD_DATE="1666400112" LAST_MODIFIED="1707309659">markdown-it demo</A>
            <DT><A HREF="https://typophile.tumblr.com/" ADD_DATE="1666619081" LAST_MODIFIED="1707309659">typophile</A>
            <DT><A HREF="https://www.onomatopee.net/exhibition/copy-this-book/" ADD_DATE="1670517530" LAST_MODIFIED="1707309659">Copy This Book | Onomatopee</A>
            <DT><A HREF="https://pagedjs.org/about/" ADD_DATE="1670517537" LAST_MODIFIED="1707309659">Paged.js — About Paged.js?</A>
            <DT><A HREF="https://pagedjs.org/posts/2020-04-15-starterkits-for-pagedjs/" ADD_DATE="1676836029" LAST_MODIFIED="1707309659">Paged.js — A collection of starter kits for Paged.js</A>
            <DT><A HREF="https://prepostprint.org/" ADD_DATE="1676835612" LAST_MODIFIED="1707309659" ICON_URI="https://prepostprint.org/site/templates/img/favicon.png" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9btaIVBzuIOGSoHcSCqIijVqEIFUKt0KqDyaVf0KQhSXFxFFwLDn4sVh1cnHV1cBUEwQ8QJ0cnRRcp8X9JoUWMB8f9eHfvcfcO8NfLTDU7xgFVs4xUIi5ksqtC8BVd6EUIo4hKzNTnRDEJz/F1Dx9f72I8y/vcn6NPyZkM8AnEs0w3LOIN4ulNS+e8TxxmRUkhPiceM+iCxI9cl11+41xw2M8zw0Y6NU8cJhYKbSy3MSsaKvEUcURRNcr3Z1xWOG9xVstV1rwnf2Eop60sc53mMBJYxBJECJBRRQllWIjRqpFiIkX7cQ//kOMXySWTqwRGjgVUoEJy/OB/8LtbMz854SaF4kDni21/jADBXaBRs+3vY9tunACBZ+BKa/krdWDmk/RaS4scAf3bwMV1S5P3gMsdYPBJlwzJkQI0/fk88H5G35QFBm6BnjW3t+Y+Th+ANHWVvAEODoFogbLXPd7d3d7bv2ea/f0Aek5yqjFhQQIAAAAGYktHRAAAAAAAAPlDu38AAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfkBgsMLALHZXJzAAABWklEQVQ4y6WTvaoiQRSEa3QRTNRMA8FEGhNTH8Jc5yUMzIwm8w18D1MjwUBQUZwBNRAEwUQ0Eg1s/JnvRrtcdx1huRVWV3/Q51Q7APqBfkUdWGu1Wq20Xq9lrZUxRpVKRYlE4jXIN1lr8X2fTqdDPp9HEpLIZDJIotlscrvdvl/hBbDZbEgmk0ii3W4TBAHX65UwDBkMBkhiPp9HAwD2+z2SGA6HL/7hcEAS0+n0xY/9/fZcLqdWq6XRaPTHez6f6na7KpVKMsZEz+C3er0exhjO5zOTyYRGo4EkxuPxP9m3gN1uhyQKhQKSqNfrLJfLd9H3gDAMqVar1Go1giDg8XgQpdi7DjiOI9d1dTqdVC6XFY/Ho5sURV4sFkhiu93ySbEocLFYVDable/7n7v8ie55Hul0Gs/zIucQCTgej6RSKVzXpd/vc7/f/w9wuVyYzWYfNwDg/PQ7fwHPhVJzZvQ3wQAAAABJRU5ErkJggg==">Home - PrePostPrint</A>
            <DT><A HREF="https://www.metaflop.com/modulator" ADD_DATE="1677422485" LAST_MODIFIED="1707309659">modulator | metaflop</A>
            <DT><A HREF="https://hallointer.net/" ADD_DATE="1679580638" LAST_MODIFIED="1707309659">hallointer.net</A>
            <DT><A HREF="http://paperjs.org/about/" ADD_DATE="1679582691" LAST_MODIFIED="1707309659">Paper.js — About</A>
            <DT><A HREF="https://www.design-research.be/about" ADD_DATE="1680195048" LAST_MODIFIED="1707309659">design research</A>
            <DT><A HREF="https://www.generativehut.com/post/how-to-make-generative-art-feel-natural" ADD_DATE="1680279002" LAST_MODIFIED="1707309659">How to make generative art feel natural</A>
            <DT><A HREF="https://sighack.com/post/generative-overlay-textures" ADD_DATE="1680279005" LAST_MODIFIED="1707309659">Generative Overlay Textures · Sighack</A>
        </DL><p>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1708557618">RISOPRINTERS</H3>
        <DL><p>
            <DT><A HREF="http://maisonriso.fr/nos-papiers/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Nos papiers - maison riso</A>
            <DT><A HREF="https://www.grandroyalstudio.fr/risographie/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">La risographie - GRAND ROYAL STUDIO</A>
            <DT><A HREF="https://www.fidele-editions.com/riso" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">IMPRESSION RISO — Fidèle</A>
            <DT><A HREF="http://www.apresmidilab.com/publications.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">AML</A>
            <DT><A HREF="http://www.colorama.space/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Colorama Risoprint</A>
            <DT><A HREF="https://www.drucken3000.de/print-templates/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Print-Templates | DRUCKEN3000</A>
            <DT><A HREF="http://www.frausteiner.be/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Printing | Frau Steiner</A>
            <DT><A HREF="http://ateliersdutoner.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Les ateliers du TONER</A>
            <DT><A HREF="https://chezrosi.wordpress.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">chez rosi | cool &amp; artisanal risograph studio bruxelles – Rosi Love Riso</A>
        </DL><p>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1708557623">EDITION</H3>
        <DL><p>
            <DT><A HREF="https://shop.selfpublishbehappy.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">SPBH Editions</A>
            <DT><A HREF="https://hahahahahahahahahahahahahaha.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Look Back and Laugh Books</A>
            <DT><A HREF="http://www.zinesofthezone.net/pages/events/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">NEXT EVENTS ⌁ Zines Of The Zone</A>
            <DT><A HREF="http://rujapress.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Ruja Press</A>
            <DT><A HREF="http://k-i-o-s-k.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Kiosk</A>
            <DT><A HREF="http://culturesmaison.be/fr/editeurs" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Editeurs | Cultures Maison</A>
            <DT><A HREF="https://www.messenger.com/messenger_media?thread_id=100005546945335&attachment_id=1118345831921704&message_id=mid.%24cAAAABxFS8CZ-m2fG0F4agoMreCAg" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Messenger</A>
            <DT><A HREF="https://www.indiecon-festival.com/festival" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Indiecon 2021 – Festival</A>
            <DT><A HREF="https://diebrueder.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Die Brueder</A>
            <DT><A HREF="https://stephencoles.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Stephen Coles</A>
        </DL><p>
    </DL><p>
    <DT><A HREF="https://www.radical-guide.com/?fbclid=IwAR1IuK_0aAlGK-q2KeDgO6TZGZSKH-pr0xUbsSMdVdbhxzLCehaScwgkmfM" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Home - A Radical Guide</A>
    <HR>    <DT><H3 ADD_DATE="1670582292" LAST_MODIFIED="1708557597">Blogs</H3>
    <DL><p>
        <DT><A HREF="https://ioannouolga.blog/2022/02/20/smells-like-connectivism-donna-haraways-concept-of-situated-knowledges/" ADD_DATE="1670582258" LAST_MODIFIED="1707309659">Smells like connectivism: Donna Haraway’s concept of situated knowledges | connecting data to information to knowledge</A>
        <DT><A HREF="https://supervalentthought.com/category/politics/" ADD_DATE="1666254063" LAST_MODIFIED="1707309659">Politics | . . . . . . . Supervalent Thought</A>
        <DT><A HREF="https://archive.org/details/terminal-escape-collection?tab=collection" ADD_DATE="1692293936" LAST_MODIFIED="1707309659">Terminal Escape : Free Audio : Free Download, Borrow and Streaming : Internet Archive</A>
        <DT><A HREF="https://terminalescape.blogspot.com/" ADD_DATE="1692293944" LAST_MODIFIED="1707309659">TERMINAL ESCAPE</A>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1708557597">Music blogs</H3>
        <DL><p>
            <DT><A HREF="http://punkarchives.blogspot.com/search?updated-max=2008-11-03T17:34:00-08:00&max-results=7&start=7&by-date=false" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Punk Archives</A>
            <DT><A HREF="http://chairshotrecords.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Chairshot To The Skull</A>
            <DT><A HREF="https://www.reddit.com/r/TheMysteriousSong/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">TheMysteriousSong</A>
            <DT><A HREF="http://dieordiy2.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Die or D.I.Y.?</A>
            <DT><A HREF="https://year-zero-records.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Year Zero Records</A>
            <DT><A HREF="https://muzika-komunika.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Muzika - Komunika</A>
            <DT><A HREF="https://tapeattack.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Tape Attack</A>
            <DT><A HREF="https://dissonance.libsyn.com/" ADD_DATE="1698501686" LAST_MODIFIED="1707309659">DISSONANCE</A>
        </DL><p>
    </DL><p>
    <DT><H3 ADD_DATE="1695466303" LAST_MODIFIED="1707309659">Recipes</H3>
    <DL><p>
        <DT><A HREF="https://www.aux-fourneaux.fr/galettes-de-courgettes-a-la-poele-47442/" ADD_DATE="1695466311" LAST_MODIFIED="1707309659">Galettes de courgettes à la poêle | Aux Fourneaux</A>
    </DL><p>
    <DT><H3 ADD_DATE="1695466337" LAST_MODIFIED="1707309659">Movies</H3>
    <DL><p>
        <DT><A HREF="https://www.justwatch.com/us/movie/so-which-band-is-your-boyfriend-in" ADD_DATE="1695466341" LAST_MODIFIED="1707309659">So, Which Band is Your Boyfriend in? streaming</A>
    </DL><p>
    <DT><H3 ADD_DATE="1665163294" LAST_MODIFIED="1707309659">Books</H3>
    <DL><p>
        <DT><A HREF="https://draw-down.com/" ADD_DATE="1665163287" LAST_MODIFIED="1707309659">Draw Down Books</A>
        <DT><A HREF="https://riot-editions.fr/" ADD_DATE="1673825665" LAST_MODIFIED="1707309659">Riot Éditions</A>
        <DT><H3 ADD_DATE="1677422048" LAST_MODIFIED="1707309659">Radical</H3>
        <DL><p>
            <DT><A HREF="https://www.editionslibertalia.com/catalogue/poche/hardi-compagnons" ADD_DATE="1677422048" LAST_MODIFIED="1707309659">Hardi, compagnons ! (Clara Schildknecht) // Les éditions Libertalia</A>
            <DT><A HREF="https://www.lepassagerclandestin.fr/" ADD_DATE="1677422048" LAST_MODIFIED="1707309659">Le Passager Clandestin - Maison d&#39;édition indépendante</A>
        </DL><p>
        <DT><A HREF="https://theanarchistlibrary.org/special/index" ADD_DATE="1677422292" LAST_MODIFIED="1707309659">The Anarchist Library | The Anarchist Library</A>
        <DT><A HREF="https://www.plutobooks.com/" ADD_DATE="1677422455" LAST_MODIFIED="1707309659">Pluto Press - Independent, Radical Publishing</A>
    </DL><p>
    <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Music</H3>
    <DL><p>
        <DT><A HREF="https://brobtilttapes.wordpress.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Brob Tilt&#39;s tapes</A>
        <DT><A HREF="http://albumsthatneverwere.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Albums That Never Were</A>
        <DT><A HREF="https://absurdexposition.bandcamp.com/album/taskmaster" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Taskmaster | Absurd Exposition</A>
        <DT><A HREF="https://factotumtapes.bandcamp.com/album/matt-weston-2" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">matt weston | factotumtapes</A>
        <DT><A HREF="https://www.reddit.com/r/experimentalmusic/comments/fjxclf/im_social_distancing_and_out_of_boredom_i/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">I&#39;m social distancing and out of boredom I organized all of my favorite current experimental labels. Mostly noise, but there&#39;s definitely a variety. Bandcamp links below. : experimentalmusic</A>
        <DT><A HREF="http://factotumtapes.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">factotum tapes</A>
        <DT><A HREF="http://wilfullyobscure.blogspot.com/2012/11/pink-lincolns-headache-1989-musical.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Wilfully Obscure: Pink Lincolns - Headache (1989, Musical Tragedies)</A>
        <DT><A HREF="http://fasterandlouderblog.blogspot.com/2011/10/great-band-you-forgot-pink-lincolns.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Faster and Louder: Great Band You Forgot: The Pink Lincolns</A>
        <DT><A HREF="http://wilfullyobscure.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Wilfully Obscure</A>
        <DT><A HREF="http://punkarchives.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Punk Archives</A>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Obscure music blogspots</H3>
        <DL><p>
            <DT><A HREF="http://sophiesfloorboard.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Sophie&#39;s Floorboard</A>
            <DT><A HREF="http://punkarchives.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Punk Archives</A>
            <DT><A HREF="http://ughh90.blogspot.com/search/label/Japan" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">90&#39;s Hip-Hop: Japan</A>
        </DL><p>
        <DT><A HREF="http://southfloridamusicscene.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">The South Florida Music Scene Past And Present</A>
        <DT><A HREF="http://floridamusicscene.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Florida Music Scene</A>
        <DT><A HREF="http://deadjonalive.blogspot.com/search?updated-max=2009-12-27T20:03:00-08:00&max-results=5" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">What We Want We Must Create</A>
        <DT><A HREF="http://punk-rock-in-ussr.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Punk Rock For Life</A>
        <DT><A HREF="http://www.screamingfastcore.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">:: Screaming Fastcore ::</A>
        <DT><A HREF="http://forivadell.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">For Ivadell</A>
        <DT><A HREF="https://ladestileriasonora4.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">La Destileria Sonora</A>
        <DT><A HREF="http://openmindsaturatedbrain.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">(((((OPENmind/SATURATEDbrain)))))</A>
        <DT><A HREF="https://discohijack.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Desperate and Lonely</A>
        <DT><A HREF="http://noisedelivery.blogspot.com/2012/08/pg-99.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Noise Delivery: Pg. 99</A>
        <DT><A HREF="http://noisedelivery.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Noise Delivery</A>
        <DT><A HREF="http://shinygreymonotone.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">[shiny grey monotone]</A>
        <DT><A HREF="http://skramzz.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">skramzz</A>
        <DT><A HREF="http://thewasteofwords.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Waste of Words</A>
        <DT><A HREF="http://www.boringemo.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Boring Emo</A>
        <DT><A HREF="https://noiseindustrialpowerelectronics.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">the noise industrial powerelectronics corner</A>
        <DT><A HREF="http://missingawarmlight.blogspot.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">missing a warm light</A>
    </DL><p>
    <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">ART CONTEMPORARY</H3>
    <DL><p>
        <DT><A HREF="http://www.realdmz.org/?ckattempt=1" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">REAL DMZ PROJECT</A>
        <DT><A HREF="https://www.afterall.org/about/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">About • Afterall</A>
        <DT><A HREF="http://www.marianalbantova.com/artist-books.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Maria Nalbantova</A>
        <DT><A HREF="https://openartfiles.bg/en" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Topics, people, spaces, files in bulgarian contemporary art | Open Art Files</A>
        <DT><A HREF="http://www.carstennicolai.de/?c=biography" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">carsten nicolai</A>
        <DT><A HREF="https://openartfiles.bg/en/topics/1389-the-art-of-books-printing-and-graphic-design" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">The Art of Books, Printing and Graphic Design | Open Art Files</A>
        <DT><A HREF="https://ificantdance.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">IF I CAN’T DANCE – I DON’T WANT TO BE PART OF YOUR REVOLUTION</A>
        <DT><A HREF="https://civa.brussels/fr" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">CIVA - Architecture, Paysage, Urbanisme BRUXELLES</A>
        <DT><A HREF="https://danielwagener.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Daniel Wagener • Photography / Print / etc.</A>
    </DL><p>
    <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Creative Code</H3>
    <DL><p>
        <DT><A HREF="https://www.audiovisualcity.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Audiovisual City - The Digital Magazine of Audiovisual Culture</A>
        <DT><A HREF="http://www.schnitt.it/biography%20marco.htm" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">marco monfardini biography</A>
        <DT><A HREF="http://www.amelieduchow.com/AMELIE%20DUCHOW%20%20input.htm" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">AMELIE DUCHOW | input</A>
        <DT><A HREF="https://jetztkultur.de/b-seite/jetztkultur/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Now culture</A>
        <DT><A HREF="https://thebookofshaders.com/01/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">The Book of Shaders: What is a shader?</A>
        <DT><A HREF="https://ogldev.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">OpenGL Step by Step - OpenGL Development</A>
        <DT><A HREF="https://www.scratchapixel.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Scratchapixel</A>
        <DT><A HREF="https://forum.processing.org/one/topic/how-to-run-another-sketch-in-current-display.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">How to run another sketch in current display? - Processing Forum</A>
        <DT><A HREF="https://openframeworks.cc/documentation/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">documentation | openFrameworks</A>
    </DL><p>
    <DT><H3 ADD_DATE="1680278982" LAST_MODIFIED="1708558219">Indieweb</H3>
    <DL><p>
        <DT><A HREF="https://indieweb.org/Getting_Started" ADD_DATE="1680278975" LAST_MODIFIED="1707309659">Getting Started - IndieWeb</A>
        <DT><A HREF="https://indiewebify.me/" ADD_DATE="1680278975" LAST_MODIFIED="1707309659">IndieWebify.Me - a guide to getting you on the IndieWeb</A>
        <DT><A HREF="https://benhoyt.com/writings/the-small-web-is-beautiful/" ADD_DATE="1680278975" LAST_MODIFIED="1707309659">The small web is beautiful</A>
        <DT><A HREF="http://luckysoap.com/statements/handmadeweb.html" ADD_DATE="1683272145" LAST_MODIFIED="1707309659">J. R. Carpenter || A Handmade Web</A>
        <DT><A HREF="https://telegra.ph/why-not-matrix-08-07" ADD_DATE="1696163976" LAST_MODIFIED="1707309659">why not matrix? – Telegraph</A>
        <DT><A HREF="https://solar.lowtechmagazine.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">LOW←TECH MAGAZINE</A>
        <DT><A HREF="https://search.marginalia.nu/" ADD_DATE="1670015050" LAST_MODIFIED="1707309659">Marginalia Search</A>
        <DT><A HREF="https://indieseek.xyz/" ADD_DATE="1670094312" LAST_MODIFIED="1707309659">Indieseek.xyz Indie Web Directory</A>
        <DT><A HREF="https://sadgrl.online/" ADD_DATE="1670173383" LAST_MODIFIED="1707309659">Working Toward a Better Internet</A>
        <DT><A HREF="https://joshuatz.com/posts/2020/firefox-backup-bookmarks-options-and-automated-scripts/" ADD_DATE="1708558219" LAST_MODIFIED="1708558219" ICON_URI="https://joshuatz.com/media/cropped-favicon3-1-32x32.png" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAoklEQVRYhWNgGOmAEZeEBaPtf3o4gIkeluADLIQUHH95mCYWW4rbMjAwDIUQgAMxKtn4CpU74CEw6gDi0wA6eIVDHD2t4FIHBUM4BKAAlp8JlRcwdehg6IcAqeDE/8Mo9c/AhwC9aj1cYOBDAMagVa1HCAyeEIADEksySsGAh8CoAwiXhNRqCeEAAx4Cow7ATAOU5nsS9Q94CDDSuzYcdO0BAHnWGJhqYEqHAAAAAElFTkSuQmCC">Firefox Backup Bookmarks Options and Automated Scripts</A>
    </DL><p>
    <DT><H3 ADD_DATE="1705865324" LAST_MODIFIED="1707309659">RasPi Music Stereo</H3>
    <DL><p>
        <DT><A HREF="https://mopidy.com/ext/raspberry-gpio/" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">Mopidy-Raspberry-GPIO</A>
        <DT><A HREF="https://www.tecmint.com/command-line-music-players-for-linux/" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">💤 The 6 Best Command Line Music Players for Linux</A>
        <DT><A HREF="https://www.adafruit.com/product/2223" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">💤 GPIO Stacking Header for Pi A+/B+/Pi 2/Pi 3 [Extra-long 2x20 Pins] : ID 2223 : $2.50 : Adafruit Industries, Unique &amp; fun DIY electronics and kits</A>
        <DT><A HREF="https://pinout.xyz/pinout/pi_dac" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">💤 Pi-DAC+ at Raspberry Pi GPIO Pinout</A>
        <DT><A HREF="https://shop.mchobby.be/fr/" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">💤 MCHobby - Vente de Raspberry Pi, Arduino, ODROID, Adafruit</A>
        <DT><A HREF="https://www.instructables.com/QuizzPi/" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">💤 QuizzPi, a Raspberry Pi Trivia Game With Python : 7 Steps (with Pictures) - Instructables</A>
        <DT><A HREF="https://www.circuitbasics.com/how-to-set-up-buttons-and-switches-on-the-raspberry-pi/" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">💤 How to Set Up Buttons and Switches on the Raspberry Pi - Circuit Basics</A>
        <DT><A HREF="https://github.com/9and3r/mopidy-ttsgpio" ADD_DATE="1705865324" LAST_MODIFIED="1707309659">GitHub - 9and3r/mopidy-ttsgpio: Extension to control mopidy without screen</A>
    </DL><p>
    <DT><A HREF="https://movieroom.is/movie/the-empire-strikes-back/" ADD_DATE="1706054810" LAST_MODIFIED="1707309659">Empire strikes back</A>
    <DT><A HREF="https://academic.oup.com/book/36469?login=false" ADD_DATE="1706054820" LAST_MODIFIED="1707309659">Theory of the Image | Oxford Academic</A>
    <DT><A HREF="https://elan.place/paragraph-club/" ADD_DATE="1706054824" LAST_MODIFIED="1707309659">Elan Kiderman Ullendorff — paragraph.club</A>
    <DT><A HREF="https://watchseries8.to/watch-series/parks-and-recreation-watch-38991/98029" ADD_DATE="1706054833" LAST_MODIFIED="1707309659">Watch Parks and Recreation (2009) Full HD</A>
    <DT><A HREF="place:parent=toolbar_____" ADD_DATE="1708557646" LAST_MODIFIED="1708557646">Bookmarks Toolbar</A>
    <DT><H3 ADD_DATE="1706966472" LAST_MODIFIED="1707397238" PERSONAL_TOOLBAR_FOLDER="true">Bookmarks Toolbar</H3>
    <DL><p>
        <DT><A HREF="https://www.mozilla.org/firefox/?utm_medium=firefox-desktop&utm_source=bookmarks-toolbar&utm_campaign=new-users&utm_content=-global" ADD_DATE="1697055523" LAST_MODIFIED="1707334484" ICON_URI="fake-favicon-uri:https://www.mozilla.org/firefox/?utm_medium=firefox-desktop&utm_source=bookmarks-toolbar&utm_campaign=new-users&utm_content=-global" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAHY0lEQVR4Aa3VA5RrSdfG8f+uOidJp/umczm2ffFhbNvG9722bdv22LZt+3I81+04B1XvfpPmWHut3yk06smus1Z4L8uXDv6MHzpowA8eWFS8FaY9eU+cCvxaFfF8W/FWGDy8a6n7DM7/H96DR3ldu0MVb8a0J+9CI1qXJP11a+79GOdP1f11FW/EtCfvQpx8mziFxMHEEEV1KYkrKl6Pea1Nbnrs/7hz7q2KUQsqRcUE/eV1acb/pyFQ7b9N3fguzNTxVsXrMa/avFgPb6SnukY8W6EgXvszrszjivH08F0VLZFK0rbUgRt9H2aS+lORznUxnTMV45kJG6fNPZSGnEodTJwUFGbphqdSll/H/SxWjEc92kYxSoO0uzEcwo90g/9rlKpHpCmX491MxQgzuvjtE0UieyqxhYZA3UGp8CjUtSMR2YrkFdf+/szi9X88+zM3/uncSx/81/f+7/HzPsu8q09i8MUNcCUHUTImceAAL+RC+UW1nMzHuUvxSVGBCloTgMT+GuOLipaGyg/OpLuE/jVI58wHb/zsdxD5tBVbDMQwOPe/8UDqHYuuPJjCZnP5nw/+mfyUPhADRtkAaIfosum23klBxH8b+KzCfuczG8IPXi4C5yHQwvDoPYhCBSkz1n9y1+WLd8xFzVUxxmIRjBIBVHXFZF58aEtW3exxsp0V8Aac8gpBnGQBRNymkP4VXKPdgdj8H2JB/DgMVwreATFhdoCdj/wY8x7+GM8/djyJ81hlnCPTUWfHb/0QlyRUelalEPcCHswIQARJPd64ohh/KHBagPcQB7sggHgIVHcM0wUyWWUAoNaEuobI9bP1dj9lw1nnMvehj/LS0wdinYO4wM1f/h6z3v9n1t3pTnAWBj04ZQA7LFROwMsu7QCpgcjuCh4Asg5Wa0ImgNDqqHTOtDyIgPPKkZ/cZOstzmT+Nw4jcA5JBO9SHjzzWKZt8CRd03ohD/RZALCigIwHawBmKgKSVoAiAi2VDCzsgo0bYB04lSojEAaQDyETsmTZf3PPdZ+irvMgTTF4SAVX7+SRC/dj5/f/C6D9d5UQLBAIFBJILIhtB1g2a8uZq+1+LwiAV8CSTujPwqoRbJjCJMdAeRVue+j/WLh4T2I3jcCEhN4ShmDFYR2IAXC8OHdDaMYAYBxU82AFAgPShHoejAEgUEViy2h5UbS9LLBajf5oMr866wc0wlWQvEEyNQKbIcSSwZBNIfAO41NQ9ZXd0IgBAQdUDAQWpjQhcfi6gCgguDtTm3vIUBdhdwUA/Pggqmy49/n/pr/q8ZMq4DziEwI0QOtpiT1kXUqQRqC8ohaDy0BqoGzxOUE6q9DwMBiOvtzm5OLi3migAFEwpjnOCzmKhZXUkyr1uEwtLqky1aStNk4jqhFFDVZb6ykYMjBodQxw5RAKZUgSqAq+YmmWzFxF0P8L61Z8pHhf5/S+bfHQJm1OLcuzw4YPcWH3/qysTcebFHyESTOkhLjUokt8M8VFCVYDbLXhvdCfARfiG3lkykDr2qhbXJTRUZBAngMwootGI3tbrbcIsR3ugp3Yhbun89l9/ko+qCDVGpQruHKJqDakBmnq2KyXaDZKrDX1KWau+ij0ZqAvgwR1JFuFmihwPTkdDQN9co3C6IMnwujs0sppELcOV+NHVc2wzv2eb+74J6ZP6kGazeEgZZJqiaRWJo6qbDb5MU7c4ixYmYUhC7YJaQxVgYrgSxa3sgNftdww31+usFuvuykfWDzU/4HytL0llTVz+SbiAScTryKxFFc6dlnnQVZP+wEo2grT7ACb5V7g2BnXsVfxHsLEgfGQTYb/1kJqWpKV3VDLM1iXi/a8PDrtqmecl451DwLg8oG1DtnMmcsKq/bQ1V3BmBTsfzgIfHucwINxICivADt8eADkBLJGtcc0ydHsmU7QEXBFfzwTeFwRnLFtDoBD7nv5+vv61v2XXzHlfR7oKtQxLkFcCqkDK8qMHdIex4gSMxaoKZBtS8lQ18NtJsPSmv/Nyfc3nma4RjsA8Jnq1HU+WC9cY01z865pJQrdDcQkrW6IpGOfun3oxLnw6m/SEBIyVFbOIMhmiXJy35oL+vYDBhkuGxY3YaTuy9TLA+Jv2inu2j2ph9NrTUMmCyIGjwEnyiCtUaUWnGlLR1hIlM6rKwpUX5qBiTuI02Du94aqx8zJhEsVI4IPduUZV+7vDC0CDv9GdeolUjObL18ckutqMKkQkc2kiFHOITLCwyiUp1bNUhuYRFrrxPoMzdDM/XbUf/gZvvYsozX+Cl5d5vh690afrk3+0hR4XyoxqYmQICaTSwjClI6cA3EIvhWi0QiIm6rRgaQh1ikfsMK43/xv8YWfASuUe6sBAIzqPmNwjb1nJdnP5PDbOpPgJMXjWhDAC4JgvEWUaQkoib/o/NzQb37S1fP0+Dt/6wHGKqe6v1yZvuG+zc69p3m7d4dnW8TjAaEdwmFKEcztkfSG67KVG346aeV8YEglincRYLQClVcdKsery6lI1VVNJbyF+jdp8gPG4E08mAAAAABJRU5ErkJggg==">Getting Started</A>
        <DT><A HREF="https://prospectivedesigneh.neocities.org/index.html" ADD_DATE="1670162150" LAST_MODIFIED="1707309659">Design Prospectif / Prospective Design ?</A>
        <DT><A HREF="https://www.2dh5.nl/" ADD_DATE="1685635349" LAST_MODIFIED="1707309659">2.Dh5 | Festival voor wereldversleutelaars</A>
        <DT><A HREF="https://acu.nl/about/" ADD_DATE="1685635351" LAST_MODIFIED="1707309659">About at ACU</A>
        <DT><A HREF="https://rss.hostux.net/i/?a=reader&rid=651971b967619" ADD_DATE="1696171025" LAST_MODIFIED="1707309659">Main stream · FreshRSS</A>
        <DT><A HREF="https://monoskop.org/Monoskop" ADD_DATE="1696679891" LAST_MODIFIED="1707309659">Monoskop</A>
        <DT><H3 ADD_DATE="1670228935" LAST_MODIFIED="1707309659">tools</H3>
        <DL><p>
            <DT><A HREF="http://txti.es/" ADD_DATE="1670228906" LAST_MODIFIED="1707309659">txti - Fast web pages for everybody</A>
            <DT><A HREF="https://no-ht.ml/" ADD_DATE="1670228949" LAST_MODIFIED="1707309659">https://no-ht.ml/</A>
            <DT><A HREF="https://housmans.com/" ADD_DATE="1670234309" LAST_MODIFIED="1707309659">Housmans Bookshop – We are a not-for-profit bookshop, specialising in books, zines, and periodicals of radical interest and progressive politics. We stock the largest range of radical newsletters, newspapers and magazines of any shop in Britain.</A>
            <DT><A HREF="https://www.reddit.com/r/FREEMEDIAHECKYEAH/wiki/reading/" ADD_DATE="1670246257" LAST_MODIFIED="1707309659">reading - FREEMEDIAHECKYEAH</A>
            <DT><A HREF="https://dodorepo.com/" ADD_DATE="1670246822" LAST_MODIFIED="1707309659">Dodo Repo</A>
            <DT><A HREF="https://indieblog.page/" ADD_DATE="1670251936" LAST_MODIFIED="1707309659">Discover the IndieWeb, one blog post at a time.</A>
            <DT><A HREF="https://satyrs.eu/linkroll/" ADD_DATE="1670581991" LAST_MODIFIED="1707309659">Linkroll | The Satyrs’ Forest 🍇</A>
            <DT><A HREF="https://www.ccru.net/abcult.htm" ADD_DATE="1670581998" LAST_MODIFIED="1707309659">Ccru- abstract culture</A>
            <DT><A HREF="https://peelopaalu.neocities.org/" ADD_DATE="1670582002" LAST_MODIFIED="1707309659">Peelopaalu - Directory</A>
            <DT><A HREF="https://pdfcpu.io/generate/booklet" ADD_DATE="1695852344" LAST_MODIFIED="1707309659">PDF processor api &amp; cli | pdfcpu</A>
            <DT><A HREF="https://www.w3.org/TR/css-anchor-position-1/" ADD_DATE="1695852363" LAST_MODIFIED="1707309659">CSS Anchor Positioning</A>
            <DT><A HREF="about:reader?url=https%3A%2F%2Fmeyerweb.com%2Feric%2Fthoughts%2F2023%2F09%2F12%2Fnuclear-anchored-sidenotes%2F" ADD_DATE="1695852368" LAST_MODIFIED="1707309659">Nuclear Anchored Sidenotes – Eric’s Archived Thoughts</A>
            <DT><A HREF="https://gwern.net/sidenote" ADD_DATE="1695852372" LAST_MODIFIED="1707309659">Sidenotes In Web Design · Gwern.net</A>
            <DT><A HREF="https://meyerweb.com/eric/thoughts/2023/09/12/nuclear-anchored-sidenotes/" ADD_DATE="1695914574" LAST_MODIFIED="1707309659">Nuclear Anchored Sidenotes – Eric’s Archived Thoughts</A>
            <DT><A HREF="https://pdfsnake.app/" ADD_DATE="1703059694" LAST_MODIFIED="1707309659">PDF Snake</A>
            <DT><A HREF="https://wiby.me/" ADD_DATE="1703973536" LAST_MODIFIED="1707309659">Wiby - Search Engine for the Classic Web</A>
        </DL><p>
        <DT><H3 ADD_DATE="1666618992" LAST_MODIFIED="1707309659">Anarchist Media</H3>
        <DL><p>
            <DT><A HREF="https://archive.org/search.php?query=anarchism&sin=&and%5B%5D=mediatype%3A%22image%22&page=2" ADD_DATE="1666618982" LAST_MODIFIED="1707309659">Internet Archive Search: anarchism</A>
            <DT><A HREF="https://libcom.org/tags/publications" ADD_DATE="1666618982" LAST_MODIFIED="1707309659">publications | libcom.org</A>
            <DT><A HREF="http://anarchief.org/wiki/WELKOM_bij_ANARCHIEF.ORG" ADD_DATE="1667562969" LAST_MODIFIED="1707309659">WELKOM bij ANARCHIEF.ORG - Anarchief</A>
            <DT><A HREF="https://www.cira.ch/" ADD_DATE="1667562999" LAST_MODIFIED="1707309659">Accueil – CIRA</A>
            <DT><A HREF="https://museum.care/" ADD_DATE="1667563084" LAST_MODIFIED="1707309659">Museum Of Care</A>
            <DT><A HREF="https://thesparrowsnest.org.uk/index.php/search" ADD_DATE="1669573267" LAST_MODIFIED="1707309659">The Sparrows&#39; Nest Library and Archive - Search</A>
            <DT><A HREF="https://anarchistnews.org/" ADD_DATE="1677422458" LAST_MODIFIED="1707309659">anarchistnews.org | We create the anarchy we&#39;d like to see in the world</A>
            <DT><A HREF="https://attack.hr/more-about-attack/" ADD_DATE="1678978021" LAST_MODIFIED="1707309659">About Attack – ACC Attack</A>
            <DT><A HREF="https://berlin-besetzt.de/#!" ADD_DATE="1695914606" LAST_MODIFIED="1707309659">Berlin Besetzt</A>
            <DT><A HREF="https://www.sproutdistro.com/" ADD_DATE="1696332651" LAST_MODIFIED="1707309659">Anarchist Zines | Sprout Distro</A>
            <DT><A HREF="https://anarchistbookfairamsterdam.blackblogs.org/anarchische-boekenbeurs-amsterdam/" ADD_DATE="1685635354" LAST_MODIFIED="1707309659">About us</A>
            <DT><A HREF="https://libcom.org/article/anarchists-and-neo-anarchists-horizontalism-and-autonomous-spaces" ADD_DATE="1697827121" LAST_MODIFIED="1707309659">Anarchists and Neo-anarchists: Horizontalism and Autonomous Spaces | libcom.org</A>
        </DL><p>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Radios</H3>
        <DL><p>
            <DT><A HREF="https://www.ma3azef.live/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">راديو معازف Ma3azef</A>
            <DT><A HREF="http://www.kanal103.com.mk/info.php?lang=1&mb=6" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" ICON_URI="http://www.kanal103.com.mk/images/103-favicon.ico" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAY6UlEQVRoQ5VaB1RU59Y99w69KNJViqBIEwVEFKxgARXsLclTkzxb7CWJYmIsf6KmmfhsifHZa4wNYyUWFMUoHZSidBCUJr3O3H+fT/HZ3r/WP2vNAoc79ztln332OVeJ3ngpiiLFxsZqnQsPD8nJyzvY0tQSN2HyxBmjR49O57/h9eZXCJ9T6x/WrFlDeCtvXfTKB/i7xNfhpXnl49Ybv/bdF2e+eT++Vnz2ljX8hbVr1+o+zs+fnJOb+yssexg4aNCK5StXXuQDYeirN2u90cv7vHCGr3l5yBtG4hJFCggIUE2bNq2drq6ucUNDQ8k///nP6nd9h6+Njo7WO3/+vHVubq5e586dK69fv/4U75Z3OoDIyHV1dYaJ8fGf3E9J2WBsYlI1cvjwbd//+OM6GN/0f0X2//M3nKNnZ2fXQ09PzxFG/v3BBx9kvctR/iwzM9Niz549AQV5eeZu3bql6evrR5WXlzfjHpp3ZuCLL76wTEtJWXft+vWZcKAldOTIw1vXrp0vWVrW4X6qN1H366+/6piZmbmp1WrjoqKiB4sXLy7/v5zp1auXmb+/vy8M8dfR0rKpqqmJRhbO/fLLL/kvvqfmbLRm8/fff+8Xfvr09PupqVY+Pj4PPTw8NsKBsnc6wDf46aefHO8nJ+8OP3N2gJautmbYsGGn14eFzeng7FyFP+u9YZx6x44dJubm5nPggENJSckOGJJ4//59+b850bt37+5wIqyludmvqbFRV6MoaYZGRl9v27ZNwBTvRkYCf5+NxP0WnPrjj09vRUdbBgUFl/l6+Q5dvmp5GtfB2xUJzxFRs7t37y6/fvXagvrGBmXokCEHx4wdu7Gmpsbd2Ni4k5asJepA0bRIsra2OicryxinDpUlyUIly1c6Ozllamtry3l5eVRZWal07dqVmpqaJFmWSaPR0NOnTx2KCgtDyisqOiHKsoGeXqmbu3u4ialpQiledWV1UXOWzHnMxMDFrqut/RWysCDlwQPTIUOGVPr5+fX/6quvUv6bAzJX2ccffxyQlJR06vHjx4QMbPH19T1jYWHxOSI9UCWrXrAH2EeWNUmJiXrPnj1rw/DCNXUe3bpVGRobSzk5OVRWVkaurq6E7IiE8E91S4txRkaGwZMnTzSoAXXbtm214WQtHKxBBu+XFJeum7twbiQul2NiYlTHjhz78cChAx8VFxcbDRo4sKb/gAED1q1bl8CE8lYG4LFOe1NT278iI8clp6SseVZZKYeGhEQNHjIks0uXLoHW1tYdcdDzDDynVQW0q0KRaeMAmjRxombsuHFNKpVKysvJUe78/Tc5u7iQp6enoFrGdUtLi3YsDNu1axcZGRnR5ClTJASoGRlsqauvL4PjF1JTU/fs27cvNjQ0lD2fduzo0bW1tbXtvby9T3h5eS1zc3N7MmnSJPWbDkgbNmwwaWxs9Iu5d+99ZGBCi1qtGhUaWtavXz+ll6+vFZwQkeR+wMYwJO7du0ezZs3SpKSkKAiA6pNPPiFAiAry8+nIkSPUzcODpkyZQq0QQsHSzRs31DNnzFABNvT9998TaFV8B1CjhISEPNzrh4KCgoM4qq6toaH/4aNHt0mS7ODn1+dzMNFufN48e/bs5lcdYLzpAwpTMlLTJ2RlPepSUlbm6OTkJCFjOLNBCQgM1Af+BAwMDAwIEaHKZ5UUFxcnrVm7RgHdUc+ePaXQkFBl8OBAAsvQjJkz6cMPP6SFCxcKh4F/Sk5Kks6dP68gAxIgSegBNGzoMHJxdVGQEQWwq83KyroHB34F1EqLHz8eFH727AzA02BYUNDXffv2PYSzSyYi2686IMMBczSLbcmJSePzcnNJjWY3ePBgat++PcXHxSnDR4xQ8GWpvq6ObGxtia/Jycklhs6p06ck/BTNq3s3D2Xc+HHk7OxMU957j+bNm0crw1ZSc3MzJScn0fXISAmNSIqLjRNOghaJMzx02FAFv2uQKQnBaACTHb4ZGVnyKDOz982bN307dOjQ0qeP3wEHe7tzNvb2NxCYxtcysH79eqdbUVHnAQnHlqYmtamFhWr0qFGU8fChAlaikSNGSHb29oQsUV9/fzaEo09dOncWWQH0JGBL0dfTI/QF6tOnDx09eoy6dXOnJUuWEhokXbp8CU4kU3VVlVSB+6BvEBOFpaWlAkxzNhQwnYIz1OjA2Qf27W+Ii4+zAGuZ6+rrK06dnR55evVI7T9w4GJ08sfCgRfyQULhDbh08WLE7ehouV2bNhpPb28VcE/nzp1T0tPTKTAwUOaociTd3d0pKipKGOOCzzp16kT4PjMMF4ioEf4MUCCknoYCIs+eVdCVK1cITCOKV5ZVlJefJ2qIvzd+/ARasHABwQFNTXUN3Yy6KZ07+yddvXaVakDHMFAyNTVTJk6aWDU8KGh8ZU1NpHAAHMv0J924cWP6iT/+2JmUnKzYdOwojxs/np5VVNDliAhxAIpRQrEKSOWD4+vr6xHVehiZKYzX0tKiqspK6uTgQD3AOmmpqVRYWEi9kYma6moRbVCnKFZBAGoN6erpUlVVlcgOZ8y/b1+cpVbAREp8QjzV1dYqZ86cofLSUpnr7llVVdN3330n9/T2/sa0omKjcADFoAI9auHGqy+eP7+8tLy8xdLCQmdkSAjdunWLUh88EAY6O7tQ2MowgqBifULgb6qtqaVjx46J4lSpZOrYsSMNAqO4gftbox0UFEQlpaUUD8zfi7lH+jAElEl2tnbk6eVJ5WXlgGkGoNaNBg4cKFguLS1N4UI3NzNTgH9CMcvGyNrj4uLm3bt3a3u4ux8wra+f01oDMiKrB34+cvd29EgzS4sGdFRD0BUdPHiQdBHZRtCbq5sbLV++nBzhAEeQjc18lEnLVywnLkjWnwsWLKA5s2dzg6PLly6J6IYgECbt2tHhQ4foiy+/FMa3adOG3n//fZoxYwbFx8cTMi/6BbNWO1ybkpyirF6zmpi2i5G5K1evUhsjY3pS8lS9f/9+FYr9Kuwd3eqAtH37dpO4mJjI9LT0Th3tbGvhgBVDAtGVTE1MqBZw8ejeXRjn6OhIHWA8QykpMUng9s6dOwSNQytWrCCOuGAovLleHFALLCc4m1+uWiV+OgBmGzdsoODgYEpJuU9nzpwWTk2aPFn8jeH206ZNAqbca04DRoZ6+tS2nYk6bOVKGXTNsmPQSxY6cOBAe6QqpayktNHGzrZKS6XqCgMUFLBsgWZTDc5HF6TJYAp2AN2QzMDhXIDM8X+j4y7//HNauGiRcOxOdLQwSAb0KsrLickAWoo2b95MmDcIqpL+QNQ5i6yZboDROFuQCdSjRw9h9MkTJ+gqRx73+f34cYJyJT9/fw2gLUHNZoOlenFrFx3+9OnTthcvXnxQW11dAYwqppATiJQSfeeObA4H2uHNEBocGEho++J3LqqYezEiA6x7Nv/8M4WEhhL0DUEqUBvUCL+4sLvDKO7EOAOUuoRpk8LDwwXdMo2iMwsH0GfEvfna27dv06YffyRbOzv6888/6RkC8cncuRo0MwnQKgMDegsH+BBMPHZ/hoffh3Qorm9o0LeztW1/8uRJAn2CukzJEwY4IPLcdLi4Oc38gg4SuAceae+ePeJw7gloQsJB/pzlAdMvN60sFD9njEXeyVOnhCPcCNkBZqK+fftRF6cugjSYurlGuLgjkSEUNm3dulXhrt3WpG1jenJ6z5cQgqa3TExISNTT1U1XaWlVIAKjIaaUmqpqKGaROnICjrn1c8PpCoP4xY2MHeiEBvftt9+KDs3ph44iQzigxu+N0D7sGMZHegJjN+K6ixcu0HfQQBwQ/iwuLh710iQ6f2sGODjz5s4FU3nRo0ePRO1s2rRJGdB/gGJuYa4BEwa8zAA6oBGMv6dSaSW4u7mmpGVkrNu+bZvGQEdP1dTcSOh8QpRxZKZOm0ZQg8IBZpD58+fTEBy8dNkyQa3MUImJicIBDCuiELnx6ejoUDX6AepN1MG0qVNFYBhiBQWF3P8EYzHcNMjiBcCNSQF4J5Yv0EMEtcB6S21vby9D9oxiB3Rhh4wbqnAQGvHFAlDlVRThDkxIGh2VlqpJ3SLkMBce5liaiAxwL+BuyxlgB9BLaCYo0NDQ8D8O4HfOBjvAMOAGxr9zY/roo4/IETA0R5e2srKi9tbWZIDr34d2YrYrQV9ZD5bau3cvjR0zRtTECUCaHXJ1cVGjUcpoimMl8LwbsCpD+Wlqqqq2nTh1qgmi7joq/JtDBw4oGF7kZo1aYL47MsA/MeAQayJ2gKGC4YLG4JDx6NxcwJyBFEgMNojrgeUzNzZtZIDrgfHMTrND/OKgsDPM/+9/8IHgfkSX0HGFXGEHGH6ckX988A8Fok5BA2xB7cyVtmzZsgHDp6xWlBacHIQI2SBaRRiaezAMcIjEOObIMiTYOC5I3EREBUpRsEVXJydOLaF+RNQfZmSQHoqWr29saETGHIUDkAnoqunQOVFkBnLgyYjhUfS4CLRsJnoIByEfswSzUyWYyQUNDrRODx8+hLP2ih7kB4LzFLR8VkJRfIkTZVjToqej42DUpk0Az7ZgD+PE+AQcqo05QiZjcDEXKnM5c7eNjY1w4CGK69q1a+SOmvBFIxNDC6LOh7EhXAMY3AXk2IFW55BhMkGDZOZh9foIHZ07+QRAkSUDZ4B1VBkkCGpP4WxxITc3NZOllWULKPp3ZDddwgrElcc/UJwaWOyIVM0uLysLqKutMz998oSCwpN1cIA9Uvwe8Mmijo13cHCEeFNRIiD09ddf07hx4wSEWh1gGmXaZIMb4ABHkWHA/+amx12Wry0qKqbAgEGEUVJ07hWQKiwp2Nj9+/bRbeig0NGjFYYjMq08K6+Q/Pr610EgjkYN5DON6gggQlVjG9EWEZn8tKhoSX1jo+Ppk6cU565OcgFSaQc5wA0IqxBkwEYM6igPHnTQyBYK57AIEEYqMJIda6VRUQMvaLQZNcAOQJCJCBcXP6HgoGFkje7NkJ0JtcuShHvDHlyza+dODDrDREdHphUtWSUFDg5MCB4xIhhBK3ttJuZdDBqMC2piL9YOPvBYM3HCBBU3HO6Yn336qUh5e+Dfy9NLpLyVRrE7JZ6FoeWfs1BCAmHX87KImUZbWQhbDNHoIN8x2FRDeveg/v37i6bIkoMVKRc/pjb6dNkyhYua5fgt2IPf5QEDB/6GWWPx0qVL698a6jF820O8/YZaGJyTna0JDh6u2rp1izAaGztqQKpN4Qw3IEGjOHQ+GhmmNZoHZmFctzYy7sRcDwyhVge4D0Sj6KtRS5WYyLhGjKEyucghmYXS5QzwvbGZUJiarSwtW3CuEnnzpgqKoKqnj89iBPQwAt7CDry6hNVCWqzN27XbZGtvPwFQ0Hh6eqlWhIWJ9s50yTTIhjGm+RBsL4SAC0Ka2RHu1Bw9ntQEpXINAELu6APcyCowIEG2iIyyIOSMMeMwNFlecAC4YXJ94FqFoQlY1iGtmqvXrhn08/fPbGdmFnD06NHHjPs3M6CaGBJibWRq+h22a1OgCjVgH62wFWFoOOaiWJlRmPo4A4x3plDurCwBWEWiQ4rDGSZsHGeDYcfygB3gQQjdUkxsTLsMKxaCvIJhlkK0heZiKY8hSbPqyy8lHW3tpJLSMv1z5891wtIs1VTfLNh3kO/Td+1GVeio1qDT75BKLBTe04AdVGErVki6iOY333wjosnd1LOHJxkZGxFPS6xIAwIDeKgXxc3sww6wDG51wIWLHo79jbmBixgrGvKGPOcXUy424aK3MFT5JxxTILM1v+3cKSEQZx88SLWAuPTu7OiYoGegPwbNr/T48eNvL7YwJTk01NUdQGPz27B+vYLql75atUr0AnYA2wSRaoaQFdo/QwiSm0agBrhrYmkr/sbzMFOj7gvuZ31TWFAg6BNyWFAx452dYunN0OLIc9a4M/N6M+LyZeXUqVMtHW1sfsZWwufIsWP9/Pv4XbUzM52688iRCvj+tgNQlt0fZTwKzy/Mt4E8VgMmWgwdjITS5599JlYqXIiMddxYTF5gB7FZO4otHHM4/45eIpoc1wArVGaSBAi/MAw9W7ZvF9Dh2djoOb8LR1jtcsfnDGBxpWCdyNkqx5JgeWlJyVhs+YaCLE526dp1HuBT+ZoDPBeAtlSRVyIDI6MiT+QX5OtgzV4PCLThoQIrFSkUOyKmMzYe45wYckygPnn/yYfysMIOjhw5kmzhHIqOHoFZpk2fLgqboXZg/z76HyhKA30Dwhki2uwY15M9eg1rLXYOfUDzYqZOQI9Ynp6WNvOPY8dGh4watb+To+PnsLWGn9K8LGJecwNTWhmpGeMjIi4fLigqrAB0srACdzl25IjBXGzXAAOJqc8WEc3OzqYmMIcB8F6GrYK+gT6xPMhFQQ4ZOlQIv7/++kvUArMUr1OSk5IpCjXz8cwZAlrcuDgzWMELB3h/xOMqSELB7kgDupUwYEVB5C27dePmkssXzo8PCgnZiVX8KtRibSuNPm/DcODChQvasffu/ePateu7yirKMxYvWnQKe8wJqQ/SHL7ChsDB0UHmYVuWZNBpI2F7TVdgJNaCwLTt8xnA0EDAiKc4drK8tAwayVeIMu6uSQmJ5N3TWxABMxLLaK4PHgaYeZh6Ufy83VMDPhLUbjLqb+HtW7cWJ8TGhg4YNGinQ+fOX4DBal7bTvNyC7jTRjOZfutm1C+g3fjZs2ZtTkhKmg0F2XvR4sWKhaWFDOhIvGlgJ2LjYlGUPwEi06gDpMBZDBxcxD54MxOxwfVYfHEPYYN5RPzrcgRBCohB/gH2TTxfBw8fLro3vzmWz7d2svrCufPyxUsX88GAi1FTH965fXsEuvBuGzu7FZAh1a+xUKsDd+/c+TAmJnYHNgt3pk6fFvakqGgeinU89IjCRkGl8gMQNBCJ0tLTxOKWR0pWlDxscOdkzHOz4wbFnZvHT3b6zOnTYoxcvXq1GA8jIRWWQZ6wCHxhPDug4akPalTCRMXbiEZk9DPANgiMNGxwQMARS2vrJRiB+XHXf1iotQaio6LGJCYn70Gh3rSwtNwNLC7AQOGIBxxt0D0NmC1evCSO4nQYy0M6NynWQhs3bhQGMaaZobAgEPMDilhBhiRejOFhCMEYrhEJQ4syFDXDkX8BZabpJtRO8batW40fpKa2xYS2BNDyw3fGDB08+G8Iy/ewnilhZ18tYgkpkaE/BgD3R2FQ5NOSkjhAasG8ufPyQkeFOuBZgSVw2vpwWqzTp06dqszF4I0DJDxw4E0er+DF7MwOMnRaHcAkJrOzDDV2IiIiggcqBaMqG//yqQ+EXjX+FnNw334bk3Ymjj26d5+fm5fX41JExFSsdUq72NoO3LR9u3ii+dp6HV1YRhPqnp2ZdbC2tiYeT09ysBedExAQeHHEyBFN2J+OACNYsHH8XYyhyg5wOh4p8TyuHDp8mLMhYSVDWLQKicAQgoxQeI5ISEzkB4gaVr3XQbHxCQkaHi0xIGGGkhSmWiyTK2BsBAr4Mv63wEgr6/a97WxtFtXU17vgO0u9vbyuQKfN++GHH8re9YxMCzDoXFdTt+fJ0+J03169ipCBGRBYe7x9fO5aW1l92rlLF3eGC5+YmZmlnZKSLGMSa8bqRAuFJaOIG9G9NQwhZqEXDkhwQMnKztbFcwaFZ2pgXIUMqgcNGtSCa0VWuU7wxPMhnkb+AOfP5mZnh1mYWwbr6+ks7WBj44hVzHoM9JucXF1/wcO/uteKuPXxP2Rvr8KCwkNGhgYR6HgX8nNyFmEv/wS7md9wUFcUpwt+Yi7VM3hcWBjw7FmlrpWVZVRZeXk/DCvGGB1Pgrtr4aTCMy2LPywAZH1d3SY8KBmChxr2gGIy6LgLZmFdiL8YCMb7IAcVKlkN+ZEFEfcXGmjxv//972893D38O9h0mGtpZtZ+7/79m7q5ukb49uz5bVJ6egaI5z81wGnFTkZ16NChsempqb85u7r+amNmtqWgpGQetLtLdze3tQuXLWPcdeLiQRRNoEgXtGvbVh9T2e42JibzMaSb342J+RSDChfYqy8Z0KsfERz8GdaE/jDydzg2FEm0ePqk5HD4n+HHEX0tfIH//0MB3k8BtfbQXjuwznFGUGahO1vAoc1enp7FA/v3X+Xs5hYBGn7tMavqX//6lyGkwFo4MNndwyMM2v94c329N0i5M7g3EvAqws0N2TLIZ203Z+dAZKIF/y0hxsPHZx50TFs8XfwRmuVNB4TAmzJx4lQ8VfFDk9uXn5s7Cc2th7VV+/PZudnbsCdt5MBAkjRi1d9ga23tcTc29mdzS0tjXD8f/3ejW8ajR1/bdOjQ4OPr+w0Cshfv1x7yYfLaaoIF667sh5nefv37zsKMewkZaYd0OqB1F4Ljn7wa1qmTJ7tmFxbWQoU+Bq3NQ5QM4dBv+N5bDmCe1QLmx0C49cHIuOvShQsfFhUXD/Dp2evq6rWrNyIbNXxv7kfANuGJvB9ky/cYSxVAdvGFc+f64BnFl9j8yYDzt2i6mzFnq/8XMjS1v9mFwAIAAAAASUVORK5CYII=">ИНФО / Kanal 103</A>
            <DT><A HREF="https://binar.bg/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Бинар - платформа за музика, изкуство и култура</A>
            <DT><A HREF="https://blackrhinomusic.ro/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Black Rhino Radio - Online Radio and Cultural Platform</A>
            <DT><A HREF="https://radiokapital.pl/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" ICON_URI="https://radiokapital.pl/static/favicons/favicon-16x16.png" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAYZQTFRFEa+uHrOxFLCvHLKwJbSzEK+uGLGwIbOyJ7W0GrGwF7GwHLCxG7CxGbCwG7GxJ7S1GLCwQmPHQWPHP2HGO42+GbGwJLOzMjHMMjDMMzHNMjHNMTHMMzLMMTDMMy/NPXXDFLGuMjPMMzPMWlrNU1POVVbOTE3NW1zOTk7NUFDNWlrOPz3NPHbCHbGxOzzMa2rPZWTNc3POZ2fNZGTNYGDNYmDNXl3OPjvNPHbDFLGvR0jOX17PQUHLYmLOT0/MdXTPXFvNXV7Oc3PPVFTMZ2XPWljNQkDNO3XCXV/Qbm/QWFnNdHXQW1vOXV3OWVnOSEnNVFTNY2PObm3QaGfQTUrNPHXCPT7MP0DMQUHMPj/LPz/MNTXMPD3NOTrMPD3MQD/MPj7MR0bORD/NQHXDFbGuTkzLTUzLTk3LTUvLUVzKQXzCNpm7E7CuRkbMR0jMQ0/KRnTEHbKwEbCuQ0HMQ0HNSU3MOaC6H66xE6+uPHrBPHnBO3nBP4DBI6qzD6+tE7KuE7Gu////bTUsngAAAAFiS0dEgRK6rv4AAAAJcEhZcwAADsMAAA7DAcdvqGQAAADGSURBVBjTY2BAAEZGJmYWBlYEYGPn4ORi4EYAHl4+fgEGQSEgEIQAYRFRMQZxcXEJSSlpcRlZcRk5eQUWBkVFRSVlFVU1dQ1NLW0dBV2QgJ6+gaGRsYmpmbmFJQODlbWNrZ29g6OTs4urmztQwMPTy9vH188/IDAoOCRUgYEhLDwiMio6JjYuPiExKTmFgSE1DQjSU9NT0zIys7JzGBhyYSAvv6CwCOiB4uKSEhB2Ky0rrwD5qBICqqprauvAXqyHgQYG7AAAQq0zI7x6glQAAAAldEVYdGRhdGU6Y3JlYXRlADIwMjAtMDItMThUMTU6NDg6MjArMDA6MDDtllJhAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIwLTAyLTE4VDE1OjQ4OjIwKzAwOjAwnMvq3QAAABF0RVh0anBlZzpjb2xvcnNwYWNlADIsdVWfAAAAIHRFWHRqcGVnOnNhbXBsaW5nLWZhY3RvcgAyeDIsMXgxLDF4MUn6prQAAABGdEVYdHNvZnR3YXJlAEltYWdlTWFnaWNrIDYuNy44LTkgMjAxNC0wNS0xMiBRMTYgaHR0cDovL3d3dy5pbWFnZW1hZ2ljay5vcmfchu0AAAAAGHRFWHRUaHVtYjo6RG9jdW1lbnQ6OlBhZ2VzADGn/7svAAAAGHRFWHRUaHVtYjo6SW1hZ2U6OmhlaWdodAAxOTIPAHKFAAAAF3RFWHRUaHVtYjo6SW1hZ2U6OldpZHRoADE5MtOsIQgAAAAZdEVYdFRodW1iOjpNaW1ldHlwZQBpbWFnZS9wbmc/slZOAAAAF3RFWHRUaHVtYjo6TVRpbWUAMTU4MjA0MDkwMIWp1HUAAAAPdEVYdFRodW1iOjpTaXplADBCQpSiPuwAAABWdEVYdFRodW1iOjpVUkkAZmlsZTovLy9tbnRsb2cvZmF2aWNvbnMvMjAyMC0wMi0xOC9lYTE0MjJiZmIxMTAxZjc3OTM4MTZlMzVkMzU0OTk2OS5pY28ucG5nFI2+hwAAAABJRU5ErkJggg==">Radio Kapitał</A>
            <DT><A HREF="https://kioskradio.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Kiosk Radio</A>
            <DT><A HREF="https://www.radiocentraal.be/Realescape/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Radio Centraal - antwerp</A>
            <DT><A HREF="https://www.radiopanik.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" ICON_URI="https://www.radiopanik.org/static/img/favicon-16.png" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAB2ElEQVQ4y4VTsYrqUBA9CQkhgRAComwRg0g6Oy2VWBmsbO2sbCzs/YCAlZY2/oBgZWHsBRtBBBtBQQubIGhlCGoyW7zdvPWZxx64xZ0zczh3Zi7oC4PBgAAQABJFkUajEZ3PZ3JdlxzHIU3TIr5arZLneURExCIGDMNAVVUkEgkkk0kUi0UYhhHxt9sNj8cDAMDFCQRBgM1mA0mSEIYhVqsV1ut1xKfTaUiS9OcS9wQAJEkSybJMsiwTx3FRXFEUmkwm32UU6wAAPM97iymKgk6nA8uy/gbjHLAsS7lcjlKp1Iuj4XBI9/udfiLWgSAIsG0bh8MBtm3DMAyYpgnLssDz/EvumwDP88hkMvj4+IBpmiiXy9B1HYqigGGY94kREQHAdDqF4zgwTRP5fB6apoHj/tuid4Hn8wmWZcGy7K9FsQL/IgxDXK9XbLdbLJdLZLNZVCoVCILwmvizo77v036/p/F4TO12mwqFAimKQgBIlmXqdrtvU4gE5vM51Wo10nWdeJ5/Warvo6oqzWazeIF+vx8lCoJArVaLer0eNRoNEkUx4prNJgVB8Pse1Ot1lEolXC4X7HY7LBYLAMDxeITv+9FfiG35lzMAwOl0guu6ESeK4st4PwHWYktdISJuuQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAASUVORK5CYII=">Radio Panik - Home</A>
            <DT><A HREF="https://threadsradio.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Threads Radio London</A>
            <DT><A HREF="https://xray.fm/shows/schedule" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Programs /// XRAY.fm</A>
            <DT><A HREF="http://stationstation.fr/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Homepage - Station Station</A>
            <DT><A HREF="https://www.radiocampus.be/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" ICON_URI="https://www.radiocampus.be/wp-content/themes/fjords/imagenes_qwilm/favicon.ico" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB5klEQVQ4T5WTQU/TYBjHf23dWhJEubB4gyxCPKgTjNmsQjxIPCBeOJIlkCjhxAE1Ksm+ARc+AhyMXojzpsY4R6pGDYtBYcOD4WAmCdRt0gVtN982bqvGkO150zRv+j6/Pv/n+b9SVQQi/ryQJMndNh1SDfA1n+dYKNQ6oFQqVW3HoVgs8sPa40RvX0sQyXGc6kvDoDccJvt5k8HzOoqitCbB1f9iJc1xAfm2vU3k5KmmIV4PXMCXrS2ujY2RF73oONLBRDzOndmbHsj9XqlUeJ5KoQYDXNR1IVP2qvQAjmMzfHWUn/v7dIW6sCwLw3jN8sMHXBoa8pInpqZ4lEwiyzL3lxa5cnm4AbDtX/SdjhDu6eb9agY9FiOVTnN9cpL+SIRDgQA3pqfrfYmPj7MwP98AuCW+efeWJ0+fMToywr1EgtVMBlXTME2Tzs6jmLumNx33bGJujtmZmQbAb6Sd3R0+rK2xZ5XRNJXvhQKH29vJ5TZZfpxkUOi/e+s2mqr+DajV5/6hIDzxcWMdRVawRX8QXtWjUVLGCm1qG+cGBupeqTvRP3irXKYsHk1I+JTdcH3O2TP9pF8ZXuKFaOxggB/mTgBxPSSx1nNZgsEg4e6e5gF+aX5w7dL9V0LTPq4ZqZWEf8/+BkDI7dH3zPe/AAAAAElFTkSuQmCC">Radio Campus</A>
            <DT><A HREF="http://listen.camp/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">CAMP / listen</A>
            <DT><A HREF="https://villabota.be/over-villa-bota/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Over ons - Villa Bota</A>
        </DL><p>
        <DT><H3 ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Fonts</H3>
        <DL><p>
            <DT><H3 ADD_DATE="1662821788" LAST_MODIFIED="1707309659">FOSS</H3>
            <DL><p>
                <DT><A HREF="http://primary-foundry.com/typefaces/happy-karrik/" ADD_DATE="1662821768" LAST_MODIFIED="1707309659" TAGS="font,typeface,typography,fonts,floss">Primary Foundry — Happy Karrik</A>
                <DT><A HREF="http://cyreal.org/" ADD_DATE="1662821816" LAST_MODIFIED="1707309659" TAGS="font,typeface,typography,fonts,floss">Cyreal Libre Fonts - Cyreal open-source fonts</A>
                <DT><A HREF="https://typotheque.luuse.fun/" ADD_DATE="1662821829" LAST_MODIFIED="1707309659" TAGS="font,typeface,typography,fonts,floss">Free Font Library</A>
                <DT><A HREF="https://www.theleagueofmoveabletype.com/" ADD_DATE="1662821838" LAST_MODIFIED="1707309659" TAGS="fonts,floss">The League of Moveable Type – the first open-source font foundry</A>
                <DT><A HREF="https://www.pepite.world/fonderie/" ADD_DATE="1663422480" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Pépite • Fonderie Typographique • Toulouse</A>
                <DT><A HREF="https://usemodify.com/" ADD_DATE="1663423783" LAST_MODIFIED="1707309659" ICON_URI="https://usemodify.com/site/templates/styles/images/favicon-use---modify.gif" ICON="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAQUlEQVQ4T2N89+7dfwYKACMuA4SEhHAaC9QDl6OdAdish7kKpwuwKUA2aNQABobRMCAhDAjlLYIpkWwDCGnEJg8A3ZiBodzt/rkAAAAASUVORK5CYII=" TAGS="fonts,floss">Home - Use &amp; Modify</A>
                <DT><A HREF="http://velvetyne.fr/" ADD_DATE="1663423789" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Home - VTF</A>
                <DT><A HREF="https://noirblancrouge.com/category-font/open-custom_fonts/" ADD_DATE="1663423944" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Open / Custom Fonts • Product categories • NoirBlancRouge</A>
                <DT><A HREF="http://www.lettres-simples.com/" ADD_DATE="1663691224" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Lettres Simples</A>
                <DT><A HREF="https://github.com/weiweihuanghuang/Prodigy-Sans" ADD_DATE="1665057543" LAST_MODIFIED="1707309659" TAGS="fonts,floss">GitHub - weiweihuanghuang/Prodigy-Sans: A grotesque sans.</A>
                <DT><A HREF="http://www.tunera.xyz/" ADD_DATE="1665061950" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Home - Tunera Type Foundry</A>
                <DT><A HREF="https://www.design-research.be/by-womxn/" ADD_DATE="1667587324" LAST_MODIFIED="1707309659" TAGS="fonts,floss">⚧ LIBRE FONTS BY WOMXN</A>
                <DT><A HREF="http://jovanny.ru/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">FREE FONTS PROJECT</A>
                <DT><A HREF="http://velvetyne.fr/hire-us/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Hire us - VTF</A>
                <DT><A HREF="http://collletttivo.it/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Collletttivo</A>
                <DT><A HREF="http://osp.kitchen/foundry/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">OSP (Open Source Publishing) →</A>
                <DT><A HREF="https://usemodify.com/page4" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Home - Use &amp; Modify</A>
                <DT><A HREF="http://copyright.rip/erg/typo/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">FDDDL/Typothèque - Acceuil</A>
                <DT><A HREF="http://typotheque.le75.be/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Typothèque Esa Septantecinq</A>
                <DT><A HREF="https://fonderie.download/index.html?fbclid=IwAR0ddgtRo73utWqjNLTmctXWwMWyAvzqi0aEw9qTnfs0O-E7eNidJoeFZA0" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">fonderie.download — Home</A>
                <DT><A HREF="http://www.love-letters.be/foundry.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Love Letters</A>
                <DT><A HREF="https://uncut.wtf/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Welcome to UNCUT</A>
                <DT><A HREF="https://republi.sh/#home" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Republish — A Vietnamese Typography Project</A>
                <DT><A HREF="https://www.are.na/frederic-brodbeck/open-source-typefaces" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">open source typefaces — Are.na</A>
                <DT><A HREF="https://www.fontshare.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Fontshare: Quality fonts. Free.</A>
                <DT><A HREF="https://open-foundry.com/fonts" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Open Foundry / Fonts</A>
                <DT><A HREF="http://fonderiz.fr/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Fond de riz</A>
                <DT><A HREF="https://u270d.eesab.fr/caracteres/" ADD_DATE="1683286008" LAST_MODIFIED="1707309659" TAGS="fonts,floss">U+270D | Caractères</A>
                <DT><A HREF="https://anrt-nancy.fr/fr/fonts" ADD_DATE="1685900229" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Anrt – Fontes</A>
                <DT><A HREF="https://permacomputing.net/" ADD_DATE="1690293146" LAST_MODIFIED="1707309659" TAGS="fonts,floss">permacomputing</A>
                <DT><A HREF="https://www.maxkohler.com/posts/eleventy-csv/" ADD_DATE="1690293152" LAST_MODIFIED="1707309659" TAGS="fonts,floss">How to use CSV data with Eleventy – Max Kohler</A>
                <DT><A HREF="https://etceteratype.co/" ADD_DATE="1705181446" LAST_MODIFIED="1707309659" TAGS="fonts,floss">Home, ETC, Etcetera Type Company</A>
            </DL><p>
            <DT><A HREF="https://www.dafont.com/satan-1981.font?text=creepshot&fpp=100&back=new" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Satan 1981 Font</A>
            <DT><A HREF="http://www.zetafonts.com/heading-pro" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Heading Pro Typeface by zetafonts - the fonts foundry</A>
            <DT><A HREF="https://www.myfonts.com/fonts/yellow-design/veneer-clean?tab=individualStyles" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Veneer Clean | Webfont &amp; Desktop font | MyFonts</A>
            <DT><A HREF="https://creativemarket.com/atk_type/4312387-Cosmo?utm_source=Link&utm_medium=CM+Social+Share&utm_campaign=Product+Social+Share&utm_content=Cosmo&ts=201911" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Cosmo ~ Display Fonts ~ Creative Market</A>
            <DT><A HREF="https://creativemarket.com/atk_type/3457787-Cygnito-Mono%E2%84%A2" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Cygnito Mono™ ~ Display Fonts ~ Creative Market</A>
            <DT><A HREF="https://cotypefoundry.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">CoType Foundry — London based type foundry of Mark Bloom and Co.</A>
            <DT><A HREF="https://www.dafont.com/it/mhtirogla.font" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Mhtirogla | dafont.com</A>
            <DT><A HREF="https://brandocorradini.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Brando Corradini</A>
            <DT><A HREF="https://g-type.com/fonts/saltaire-font" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Saltaire | G-Type Foundry</A>
            <DT><A HREF="https://g-type.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Font Foundry &amp; Custom Fonts | G-Type Foundry</A>
            <DT><A HREF="https://www.swisstypefaces.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Swiss Typefaces</A>
            <DT><A HREF="https://www.colophon-foundry.org/typefaces/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Colophon Foundry</A>
            <DT><A HREF="https://lettersfromsweden.se/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Letters from Sweden</A>
            <DT><A HREF="https://radimpesko.com/fonts" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">RP - Digital Type Foundry</A>
            <DT><A HREF="http://atipofoundry.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">fonts | atipo foundry</A>
            <DT><A HREF="https://www.vanschneider.com/best-type-foundries-to-find-typefaces" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">My Favorite Type Foundries to Find Typefaces - DESK Magazine</A>
            <DT><A HREF="https://www.abcdinamo.com/typefaces" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">DINAMO: Typefaces</A>
            <DT><A HREF="https://www.optimo.ch/typefaces.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Optimo Type Foundry Typefaces</A>
            <DT><A HREF="https://vj-type.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Typefaces - VJ-TYPE</A>
            <DT><A HREF="https://boulevardlab.com/Typefaces" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Typefaces - Boulevard LAB</A>
            <DT><A HREF="https://www.typedepot.com/fonts/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Fonts | typеdepot</A>
            <DT><A HREF="https://www.losttype.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Lost Type Co-op</A>
            <DT><A HREF="http://bonjourmonde.net/#air19" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Bonjour ( ) Monde</A>
            <DT><A HREF="https://www.toormix.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Agencia de Branding de Barcelona, Estudio de Diseño Gráfico | TOORMIX</A>
            <DT><A HREF="https://tightype.com/information/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">TIGHTYPE   Information</A>
            <DT><A HREF="https://www.generativefonts.xyz/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">NaNGlyphFilters — Generative Fonts &amp; Scripts — Home</A>
            <DT><A HREF="https://www.pepite-collectif.com/portfolio/moche/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Moche - Pépite collectif</A>
            <DT><A HREF="https://black-foundry.com/fonts/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Fonts |</A>
            <DT><A HREF="https://rsms.me/inter/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Inter font family</A>
            <DT><A HREF="https://www.awwwards.com/awwwards/collections/free-fonts/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Free Fonts - Awwwards</A>
            <DT><A HREF="https://ohnotype.co/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">OH no Type Company</A>
            <DT><A HREF="https://justanotherfoundry.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Just Another Foundry</A>
            <DT><A HREF="https://tightype.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">TIGHTYPE™</A>
            <DT><A HREF="https://thedesignersfoundry.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">The Designers Foundry | Typefaces and Fonts</A>
            <DT><A HREF="http://phantom-foundry.com/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Phantom – Type Design</A>
            <DT><A HREF="https://www.lift-type.fr/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">Lift Type — A french digital type foundry</A>
            <DT><A HREF="https://pads.erg.be/p/efd9e1juqh4vjt5d" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">efd9e1juqh4vjt5d | ErgPad</A>
            <DT><A HREF="http://la-perruque.org/order.html" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">La Perruque – Order</A>
            <DT><A HREF="https://github.com/raphaelbastide/ofont" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">GitHub - raphaelbastide/ofont: Listing fonts? YEAAHRR.</A>
            <DT><A HREF="https://opentype.js.org/" ADD_DATE="1661714161" LAST_MODIFIED="1707309659">opentype.js – JavaScript parser/writer for OpenType and TrueType fonts.</A>
        </DL><p>
        <DT><H3 ADD_DATE="1696680118" LAST_MODIFIED="1707309659">Maps</H3>
        <DL><p>
            <DT><A HREF="https://umap.openstreetmap.fr/en/map/autonomous-culture-points_915073#5/47.010/9.141" ADD_DATE="1684328255" LAST_MODIFIED="1707309659">autonomous culture points - uMap</A>
            <DT><A HREF="https://umap.openstreetmap.fr/en/map/autonomous-cultural-spaces_923171" ADD_DATE="1685629233" LAST_MODIFIED="1707309659">autonomous cultural spaces</A>
        </DL><p>
        <DT><A HREF="https://www.anarchaserver.org/" ADD_DATE="1696680707" LAST_MODIFIED="1707309659">anarchaserver</A>
        <DT><A HREF="https://yewtu.be/" ADD_DATE="1696708015" LAST_MODIFIED="1707309659">Invidious - search Youtube</A>
        <DT><A HREF="https://hostux.network/en/" ADD_DATE="1696708046" LAST_MODIFIED="1707309659">Hostux</A>
    </DL><p>
    <DT><H3 ADD_DATE="1706966472" LAST_MODIFIED="1708557528" UNFILED_BOOKMARKS_FOLDER="true">Other Bookmarks</H3>
    <DL><p>
        <DT><A HREF="https://libcom.org/article/advertising-anarchy" ADD_DATE="1669592338" LAST_MODIFIED="1707309659">Advertising Anarchy | libcom.org</A>
        <DT><A HREF="https://libcom.org/article/londons-radical-printmaking-workshops-1960s-and-70s-jess-baines" ADD_DATE="1669592330" LAST_MODIFIED="1707309659">London&#39;s radical printmaking workshops of the 1960s and 70s - Jess Baines | libcom.org</A>
        <DT><A HREF="http://garagecollective.blogspot.com/2010/04/radical-design-of-gerd-arntz.html" ADD_DATE="1669592619" LAST_MODIFIED="1707309659">Jared Davidson: The radical design of Gerd Arntz</A>
        <DT><A HREF="http://www.gerdarntz.org/index.html" ADD_DATE="1669673936" LAST_MODIFIED="1707309659">Gerd Arntz Web Archive</A>
        <DT><A HREF="https://pramen.io/en/" ADD_DATE="1688812199" LAST_MODIFIED="1708557528">Pramen - Anarchism in Belarus</A>
        <DT><A HREF="https://www.footprinters.co.uk/" ADD_DATE="1688918284" LAST_MODIFIED="1708557528">Footprint Workers Co-op – Environmentally conscious, radical riso printers in Leeds</A>
        <DT><A HREF="https://dev.luuse.fun/presentation-paf/" ADD_DATE="1686168694" LAST_MODIFIED="1708557528">Paf slide</A>
        <DT><A HREF="https://www.kalinka-m.org/" ADD_DATE="1688590592" LAST_MODIFIED="1708557528">Terminkalender - Kalinka</A>
    </DL><p>
</DL>
