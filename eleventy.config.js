const pluginRss = require("@11ty/eleventy-plugin-rss");
const { DateTime } = require("luxon");

module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy("_source/css/");
  eleventyConfig.addPassthroughCopy("_source/img/");
  // Add collection for last three rants
  eleventyConfig.addCollection("lastrants", 
	function (collectionApi) 
	{
		return collectionApi.getFilteredByTags("rant").slice(0, 3);  
	}
  );

  eleventyConfig.addFilter("postDate", (dateObj) => {
    return DateTime.fromJSDate(dateObj).toFormat('MMM dd, yyyy');
  });
  eleventyConfig.addPlugin(pluginRss);
  
  // Return your Object options:
  return {
	  
	markdownTemplateEngine: "njk",
	
    dir: {
	  includes: "_includes",
      input: "_source",
      output: "_dist"
    } 
  }
};
